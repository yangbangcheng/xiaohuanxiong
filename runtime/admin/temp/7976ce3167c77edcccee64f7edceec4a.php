<?php /*a:2:{s:61:"/etc/nginx/html/xiaohuanxiong/app/admin/view/login/login.html";i:1649902077;s:58:"/etc/nginx/html/xiaohuanxiong/app/admin/view/pub/base.html";i:1649902077;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/static/admin/images/favicon.ico" rel="icon">
    <link rel="stylesheet" href="/static/admin/libs/layui/css/layui.css"/>
    <link rel="stylesheet" href="/static/admin/module/admin.css?v=318"/>
    
<title>后台登录</title>
<style>
    body {
        background-image: url("/static/admin/images/bg-login.jpg");
        background-repeat: no-repeat;
        background-size: cover;
        min-height: 100vh;
    }

    body:before {
        content: "";
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .login-wrapper {
        max-width: 420px;
        padding: 20px;
        margin: 0 auto;
        position: relative;
        box-sizing: border-box;
        z-index: 2;
    }

    .login-wrapper > .layui-form {
        padding: 25px 30px;
        background-color: #fff;
        box-shadow: 0 3px 6px -1px rgba(0, 0, 0, 0.19);
        box-sizing: border-box;
        border-radius: 4px;
    }

    .login-wrapper > .layui-form > h2 {
        color: #333;
        font-size: 18px;
        text-align: center;
        margin-bottom: 25px;
    }

    .login-wrapper > .layui-form > .layui-form-item {
        margin-bottom: 25px;
        position: relative;
    }

    .login-wrapper > .layui-form > .layui-form-item:last-child {
        margin-bottom: 0;
    }

    .login-wrapper > .layui-form > .layui-form-item > .layui-input {
        height: 46px;
        line-height: 46px;
        border-radius: 2px !important;
    }

    .login-wrapper .layui-input-icon-group > .layui-input {
        padding-left: 46px;
    }

    .login-wrapper .layui-input-icon-group > .layui-icon {
        width: 46px;
        height: 46px;
        line-height: 46px;
        font-size: 20px;
        color: #909399;
        position: absolute;
        left: 0;
        top: 0;
        text-align: center;
    }

    .login-wrapper > .layui-form > .layui-form-item.login-captcha-group {
        padding-right: 135px;
    }

    .login-wrapper > .layui-form > .layui-form-item.login-captcha-group > .login-captcha {
        height: 46px;
        width: 120px;
        cursor: pointer;
        box-sizing: border-box;
        border: 1px solid #e6e6e6;
        border-radius: 2px !important;
        position: absolute;
        right: 0;
        top: 0;
    }

    .login-wrapper > .layui-form > .layui-form-item > .layui-form-checkbox {
        margin: 0 !important;
        padding-left: 25px;
    }

    .login-wrapper > .layui-form > .layui-form-item > .layui-form-checkbox > .layui-icon {
        width: 15px !important;
        height: 15px !important;
    }

    .login-wrapper > .layui-form .layui-btn-fluid {
        height: 48px;
        line-height: 48px;
        font-size: 16px;
        border-radius: 2px !important;
    }

    .login-wrapper > .layui-form > .layui-form-item.login-oauth-group > a > .layui-icon {
        font-size: 26px;
    }

    .login-copyright {
        color: #eee;
        padding-bottom: 20px;
        text-align: center;
        position: relative;
        z-index: 1;
    }

    @media screen and (min-height: 550px) {
        .login-wrapper {
            margin: -250px auto 0;
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            width: 100%;
        }

        .login-copyright {
            position: absolute;
            bottom: 0;
            right: 0;
            left: 0;
        }
    }

    .layui-btn {
        background-color: #5FB878;
        border-color: #5FB878;
    }

    .layui-link {
        color: #5FB878 !important;
    }
</style>

</head>
<body>

<div class="login-wrapper layui-anim layui-anim-scale">
    <form id="loginSubmit" lay-filter="loginSubmit" class="layui-form">
        <h2>后台登录</h2>
        <div class="layui-form-item layui-input-icon-group">
            <i class="layui-icon layui-icon-username"></i>
            <input class="layui-input" name="username" placeholder="请输入登录账号" autocomplete="off"
                   lay-verType="tips" lay-verify="required" required/>
        </div>
        <div class="layui-form-item layui-input-icon-group">
            <i class="layui-icon layui-icon-password"></i>
            <input class="layui-input" name="password" placeholder="请输入登录密码" type="password"
                   lay-verType="tips" lay-verify="required" required/>
        </div>
        <div class="layui-form-item layui-input-icon-group login-captcha-group">
            <i class="layui-icon layui-icon-auz"></i>
            <input class="layui-input" name="captcha" placeholder="请输入验证码" autocomplete="off"
                   lay-verType="tips" lay-verify="required" required/>
            <img class="login-captcha" src="<?php echo adminurl('login/captcha'); ?>"/>
        </div>

        <div class="layui-form-item">
            <button class="layui-btn layui-btn-fluid" lay-filter="loginSubmit" lay-submit>登录</button>
        </div>
        <div class="layui-form-item login-oauth-group text-center">
            <a href="javascript:;"><i class="layui-icon layui-icon-login-qq" style="color:#3492ed;"></i></a>&emsp;
            <a href="javascript:;"><i class="layui-icon layui-icon-login-wechat" style="color:#4daf29;"></i></a>&emsp;
            <a href="javascript:;"><i class="layui-icon layui-icon-login-weibo" style="color:#CF1900;"></i></a>
        </div>
    </form>
</div>
<div class="login-copyright">copyright © 2020 小浣熊活码系统 all rights reserved.</div>

<!-- js部分 -->
<script type="text/javascript" src="/static/admin/libs/layui/layui.js"></script>
<script type="text/javascript" src="/static/admin/js/common.js"></script>
<script>
    function subResHandle(res) {
        if (res.err == 0) {
            layer.msg(res.msg, {icon: 1, time: 1000}, function () {
                //刷新父页面
                //window.parent.location.reload();
                //关闭当前弹窗
                // var index = parent.layer.getFrameIndex(window.name);
                // parent.layer.close(index);
                window.location.reload()
            });
        } else {
            layer.msg(res.msg, {icon: 2, time: 1000});
        }
    }
</script>

<script>
    layui.use(['layer', 'form'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;

        /* 表单提交 */
        form.on('submit(loginSubmit)', function (obj) {
            $.ajax({
                type: 'POST',
                url: "<?php echo adminurl('login/login'); ?>",
                data: $('#loginSubmit').serialize(),
                dataType: 'json',
                success: function (res) {
                    if (res.err == 0){ //登录成功
                        layer.msg(res.msg, {icon: 1, time: 1000});
                        setTimeout(function () {
                            location.href = "<?php echo adminurl('index/index'); ?>";
                        }, 1000);
                    } else {
                        layer.msg(res.msg, {icon: 2, time: 1000});
                    }
                },
                error: function (data) {
                    layer.msg(data.msg, {icon: 2, time: 1000});
                },
            });
            return false;
        });

        var captchaUrl = '<?php echo adminurl("login/captcha"); ?>';
        $('img.login-captcha').click(function () {
            this.src = captchaUrl + '?t=' + (new Date).getTime();
        }).trigger('click');
    });
</script>

</body>
</html>