<?php /*a:2:{s:61:"/etc/nginx/html/xiaohuanxiong/app/admin/view/areas/index.html";i:1649902077;s:58:"/etc/nginx/html/xiaohuanxiong/app/admin/view/pub/base.html";i:1649902077;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/static/admin/images/favicon.ico" rel="icon">
    <link rel="stylesheet" href="/static/admin/libs/layui/css/layui.css"/>
    <link rel="stylesheet" href="/static/admin/module/admin.css?v=318"/>
    
<title>地区管理</title>
<style>
    /** 数据表格中的select尺寸调整 */
    .layui-table-view .layui-table-cell .layui-select-title .layui-input {
        height: 28px;
        line-height: 28px;
    }

    .layui-table-view [lay-size="lg"] .layui-table-cell .layui-select-title .layui-input {
        height: 40px;
        line-height: 40px;
    }

    .layui-table-view [lay-size="lg"] .layui-table-cell .layui-select-title .layui-input {
        height: 40px;
        line-height: 40px;
    }

    .layui-table-view [lay-size="sm"] .layui-table-cell .layui-select-title .layui-input {
        height: 20px;
        line-height: 20px;
    }

    .layui-table-view [lay-size="sm"] .layui-table-cell .layui-btn-xs {
        height: 18px;
        line-height: 18px;
    }
</style>

</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <!-- 表格工具栏 -->
            <form class="layui-form toolbar table-tool-mini">
                <div class="layui-form-item">
                    <div class="layui-inline" style="padding-right: 110px;">
                        <button class="layui-btn icon-btn" type="button" id="create">
                            <i class="layui-icon"></i>新增
                        </button>
                    </div>
                </div>
            </form>
            <!-- 数据表格 -->
            <table id="tbBasicTable" lay-filter="tbBasicTable"></table>
        </div>
    </div>

    <div class="layui-card">
        <div class="layui-card-body">
            交流和反馈建议请加QQ群：780362399
        </div>
    </div>

    <!-- 表格操作列 -->
    <script type="text/html" id="tbBasicTbBar">
        <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="edit">修改</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>


<!-- js部分 -->
<script type="text/javascript" src="/static/admin/libs/layui/layui.js"></script>
<script type="text/javascript" src="/static/admin/js/common.js"></script>
<script>
    function subResHandle(res) {
        if (res.err == 0) {
            layer.msg(res.msg, {icon: 1, time: 1000}, function () {
                //刷新父页面
                //window.parent.location.reload();
                //关闭当前弹窗
                // var index = parent.layer.getFrameIndex(window.name);
                // parent.layer.close(index);
                window.location.reload()
            });
        } else {
            layer.msg(res.msg, {icon: 2, time: 1000});
        }
    }
</script>

<script>
    layui.use(['table', 'util', 'index', 'dropdown', 'form'], function () {
        var $ = layui.jquery
        var table = layui.table
        var util = layui.util
        var index = layui.index
        var dropdown = layui.dropdown
        var form = layui.form

        $('#create').click(function () {
            index.openTab({
                title: '新建地区',
                url: "<?php echo adminurl('areas/create'); ?>",
                end: function () {
                    insTb.reload();
                }
            })
        })

        /* 表格工具条点击事件 */
        table.on('tool(tbBasicTable)', function (obj) {
            var data = obj.data; // 获得当前行数据
            if (obj.event === 'edit') { // 修改
                parent.layui.index.openTab({
                    title: '编辑题材',
                    url: "<?php echo adminurl('areas/edit'); ?>?id=" + data.id,
                    end: function () {
                        insTb.reload();
                    }
                });
            } else if (obj.event === 'del') { // 删除
                layer.confirm('确认要删除吗？', function (index) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo adminurl('delete'); ?>",
                        data: {id: data.id},
                        dataType: 'json',
                        success: function (res) {
                            if (res.err == 0) { //删除成功
                                layer.msg(res.msg, {icon: 1, time: 1000}, function () {
                                    location.reload();
                                });
                            } else {
                                layer.msg(res.msg, {icon: 2}, function () {
                                    location.reload();
                                });
                            }
                        },
                        error: function (res) {
                            layer.msg(res);
                        },
                    });
                });
            }
            dropdown.hideAll();
        });

        /* 渲染表格 */
        var insTb = table.render({
            elem: '#tbBasicTable',
            url: "<?php echo adminurl('areas/list'); ?>",
            page: true,
            cellMinWidth: 100,
            cols: [[
                {type: 'checkbox'},
                {field: 'id', title: 'ID', align: 'center', sort: true},
                {field: 'area_name', title: '地区', align: 'center', sort: true},
                {
                    field: 'create_time', title: '创建时间', templet: function (d) {
                        return util.toDateString(d.create_time * 1000, "yyyy-MM-dd");
                    }, align: 'center', sort: true
                },
                {
                    field: 'update_time', title: '更新时间', templet: function (d) {
                        return util.toDateString(d.update_time * 1000, "yyyy-MM-dd");
                    }, align: 'center', sort: true
                },
                {title: '操作', toolbar: '#tbBasicTbBar', align: 'center', minWidth: 200}
            ]]
        });
    });
</script>

</body>
</html>