<?php /*a:2:{s:70:"/etc/nginx/html/xiaohuanxiong/app/admin/view/friendshiplinks/didi.html";i:1649902077;s:58:"/etc/nginx/html/xiaohuanxiong/app/admin/view/pub/base.html";i:1649902077;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/static/admin/images/favicon.ico" rel="icon">
    <link rel="stylesheet" href="/static/admin/libs/layui/css/layui.css"/>
    <link rel="stylesheet" href="/static/admin/module/admin.css?v=318"/>
    
<title>滴滴友链设置</title>
<style>
    #formBasForm {
        max-width: 700px;
        margin: 30px auto;
    }

    #formBasForm .layui-form-item {
        margin-bottom: 25px;
    }
</style>

</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <!-- 表单开始 -->
            <form class="layui-form" id="formBasForm" lay-filter="formBasForm">
                <div class="layui-form-item">
                    <label class="layui-form-label layui-form-required">站点ID:</label>
                    <div class="layui-input-block">
                        <input name="siteid" value="<?php echo htmlentities($siteid); ?>" class="layui-input"
                               lay-verType="tips" lay-verify="required" required/>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label layui-form-required">token:</label>
                    <div class="layui-input-block">
                        <input name="token" value="<?php echo htmlentities($token); ?>" class="layui-input"
                               lay-verType="tips" lay-verify="required" required/>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-filter="formBasSubmit" lay-submit>&emsp;提交&emsp;</button>
                        <button type="reset" class="layui-btn layui-btn-primary">&emsp;重置&emsp;</button>
                    </div>
                </div>
            </form>
            <!-- //表单结束 -->
        </div>
    </div>
</div>

<!-- js部分 -->
<script type="text/javascript" src="/static/admin/libs/layui/layui.js"></script>
<script type="text/javascript" src="/static/admin/js/common.js"></script>
<script>
    function subResHandle(res) {
        if (res.err == 0) {
            layer.msg(res.msg, {icon: 1, time: 1000}, function () {
                //刷新父页面
                //window.parent.location.reload();
                //关闭当前弹窗
                // var index = parent.layer.getFrameIndex(window.name);
                // parent.layer.close(index);
                window.location.reload()
            });
        } else {
            layer.msg(res.msg, {icon: 2, time: 1000});
        }
    }
</script>

<script>
    layui.use(['layer', 'form', 'laydate', 'upload'], function () {
        var $ = layui.jquery;
        var form = layui.form;
        var laydate = layui.laydate;

        /* 渲染laydate */
        laydate.render({
            elem: '#formBasDateSel',
            trigger: 'click',
            range: true
        });

        /* 监听表单提交 */
        form.on('submit(formBasSubmit)', function (data) {
            $.ajax({
                url:"<?php echo adminurl('didi'); ?>",
                type:'post',
                data:$('form').serialize(),
                success(res) {
                    subResHandle(res)
                }
            })
            return false;
        });
    });
</script>

</body>
</html>