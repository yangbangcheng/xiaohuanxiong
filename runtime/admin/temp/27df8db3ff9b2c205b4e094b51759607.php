<?php /*a:2:{s:60:"/etc/nginx/html/xiaohuanxiong/app/admin/view/index/info.html";i:1649902077;s:58:"/etc/nginx/html/xiaohuanxiong/app/admin/view/pub/base.html";i:1649902077;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/static/admin/images/favicon.ico" rel="icon">
    <link rel="stylesheet" href="/static/admin/libs/layui/css/layui.css"/>
    <link rel="stylesheet" href="/static/admin/module/admin.css?v=318"/>
    
<title>Dashboard</title>
<style>
    /** 应用快捷块样式 */
    .console-app-group {
        padding: 16px;
        border-radius: 4px;
        text-align: center;
        background-color: #fff;
        cursor: pointer;
        display: block;
    }

    .console-app-group .console-app-icon {
        width: 32px;
        height: 32px;
        line-height: 32px;
        margin-bottom: 6px;
        display: inline-block;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        font-size: 32px;
        color: #69c0ff;
    }

    .console-app-group:hover {
        box-shadow: 0 0 15px rgba(0, 0, 0, .08);
    }

    /** //应用快捷块样式 */

    /** 小组成员 */
    .console-user-group {
        position: relative;
        padding: 10px 0 10px 60px;
    }

    .console-user-group .console-user-group-head {
        width: 32px;
        height: 32px;
        position: absolute;
        top: 50%;
        left: 12px;
        margin-top: -16px;
        border-radius: 50%;
    }

    .console-user-group .layui-badge {
        position: absolute;
        top: 50%;
        right: 8px;
        margin-top: -10px;
    }

    .console-user-group .console-user-group-name {
        line-height: 1.2;
    }

    .console-user-group .console-user-group-desc {
        color: #8c8c8c;
        line-height: 1;
        font-size: 12px;
        margin-top: 5px;
    }

    /** 卡片轮播图样式 */
    .admin-carousel .layui-carousel-ind {
        position: absolute;
        top: -41px;
        text-align: right;
    }

    .admin-carousel .layui-carousel-ind ul {
        background: 0 0;
    }

    .admin-carousel .layui-carousel-ind li {
        background-color: #e2e2e2;
    }

    .admin-carousel .layui-carousel-ind li.layui-this {
        background-color: #999;
    }

    /** 广告位轮播图 */
    .admin-news .layui-carousel-ind {
        height: 45px;
    }

    .admin-news a {
        display: block;
        line-height: 70px;
        text-align: center;
    }

    /** 最新动态时间线 */
    .layui-timeline-dynamic .layui-timeline-item {
        padding-bottom: 0;
    }

    .layui-timeline-dynamic .layui-timeline-item:before {
        top: 16px;
    }

    .layui-timeline-dynamic .layui-timeline-axis {
        width: 9px;
        height: 9px;
        left: 1px;
        top: 7px;
        background-color: #cbd0db;
    }

    .layui-timeline-dynamic .layui-timeline-axis.active {
        background-color: #0c64eb;
        box-shadow: 0 0 0 2px rgba(12, 100, 235, .3);
    }

    .dynamic-card-body {
        box-sizing: border-box;
        overflow: hidden;
    }

    .dynamic-card-body:hover {
        overflow-y: auto;
        padding-right: 9px;
    }

    /** 优先级徽章 */
    .layui-badge-priority {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        padding: 0;
        line-height: 18px;
        border-width: 2px;
        font-weight: 600;
    }
</style>

</head>
<body>

<div class="layui-fluid ew-console-wrapper">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-xs12 layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    今日订单<span class="layui-badge layui-badge-green pull-right">日</span>
                </div>
                <div class="layui-card-body">
                    <p class="lay-big-font"><?php echo htmlentities($orders_today); ?></p>
                    <p>总订单<span class="pull-right"><?php echo htmlentities($orders); ?></span></p>
                </div>
            </div>
        </div>
        <div class="layui-col-xs12 layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    今日支付订单<span class="layui-badge layui-badge-green pull-right">日</span>
                </div>
                <div class="layui-card-body">
                    <p class="lay-big-font"><?php echo htmlentities($order_payed_today); ?></p>
                    <p>总支付订单<span class="pull-right"><?php echo htmlentities($order_payed); ?></span></p>
                </div>
            </div>
        </div>
        <div class="layui-col-xs12 layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    今日注册用户<span class="layui-badge layui-badge-green pull-right">日</span>
                </div>
                <div class="layui-card-body">
                    <p class="lay-big-font"><?php echo htmlentities($user_today); ?></p>
                    <p>总注册用户<span class="pull-right"><?php echo htmlentities($user); ?></span></p>
                </div>
            </div>
        </div>
        <div class="layui-col-xs12 layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    今日充值<span class="layui-badge layui-badge-green pull-right">日</span>
                </div>
                <div class="layui-card-body">
                    <p class="lay-big-font"><?php echo htmlentities($charge_today); ?></p>
                    <p>总充值<span class="pull-right"><?php echo htmlentities($charge); ?></span></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- js部分 -->
<script type="text/javascript" src="/static/admin/libs/layui/layui.js"></script>
<script type="text/javascript" src="/static/admin/js/common.js"></script>
<script>
    function subResHandle(res) {
        if (res.err == 0) {
            layer.msg(res.msg, {icon: 1, time: 1000}, function () {
                //刷新父页面
                //window.parent.location.reload();
                //关闭当前弹窗
                // var index = parent.layer.getFrameIndex(window.name);
                // parent.layer.close(index);
                window.location.reload()
            });
        } else {
            layer.msg(res.msg, {icon: 2, time: 1000});
        }
    }
</script>

</body>
</html>