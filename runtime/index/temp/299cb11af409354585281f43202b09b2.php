<?php /*a:3:{s:38:"./template/default/pc/index/index.html";i:1669086131;s:35:"./template/default/pc/pub/base.html";i:1669263232;s:37:"./template/default/pc/pub/others.html";i:1665224102;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="baidu-site-verification" content="code-QrWB2ibm17" />
    <meta name="shenma-site-verification" content="0a1dd4a9539d78a516530b63b949795b_1669263148">
    <link rel="shortcut icon" href="/static/images/favicon.ico">
    <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon.ico">
    <meta name="applicable-device" content="pc,mobile">
    <meta name="Keywords" content="免费国漫,免费韩漫,免费日漫,免费漫画,聚合漫画,<?php echo htmlentities($site_name); ?>">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo htmlentities($current_url); ?>"/>
    <meta property="og:site_name" content="<?php echo htmlentities($site_name); ?>"/>
    <meta property="og:image" content="/static/images/favicon.ico"/>
    <script type="application/ld+json">{"@context": "https://schema.org","@type": "Organization","url": "<?php echo htmlentities($current_url); ?>","logo": "https://www.52hah.com/static/images/favicon.ico"}</script>


    
<title><?php echo htmlentities($site_name); ?> - 最新热门免费漫画在线观看 - 好看的热门漫画 - 下拉式漫画阅读 - <?php echo htmlentities($site_name); ?></title>
<meta property="og:title" content="<?php echo htmlentities($site_name); ?> - 最新热门免费漫画在线观看 - 好看的热门漫画 - 下拉式漫画阅读 - <?php echo htmlentities($site_name); ?>"/>

<meta property="og:description" content= "<?php echo htmlentities($site_name); ?>为大家带来最新最全免费漫画大全，这里有超多漫画资源，无论是国产动漫 韩国漫画，耽美漫画，还是恐怖漫画全都免费看，总有一个让你酣畅淋漓" />
<meta content="<?php echo htmlentities($site_name); ?>为大家带来最新最全免费漫画大全，这里有超多漫画资源，无论是国产动漫 韩国漫画，耽美漫画，还是恐怖漫画全都免费看，总有一个让你酣畅淋漓" name="description">

    <link href="/static/css/style.css" rel="stylesheet" type="text/css">
<!--    <link href="/static/css/userinfo-vendor.css" rel="stylesheet" type="text/css">-->

    <link href="/static/css/dm5_style.css" rel="stylesheet" type="text/css">
    
    <script src="https://lib.baomitu.com/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://lib.baomitu.com/vue/2.6.10/vue.min.js"></script>
    <script src="https://lib.baomitu.com/axios/0.19.2/axios.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery_lazyload/1.9.7/jquery.lazyload.min.js"></script>
    <script src="/static/js/index.js"></script>

</head>

<body class="white">
<!-- 页面头部 -->
<header class="header container-fluid " style="left: 0px;">
    <div class="container">
        <!-- 左侧logo -->
        <a href="/">
            <!--            <p>聚合漫画屋</p>-->
            <img class="header-logo" alt="<?php echo htmlentities($site_name); ?>" src="/static/images/header-logo.png">
        </a>
        <!-- 左侧菜单标题 -->
        <ul class="header-title">
            <li><a href="/">首页</a></li>
            <li><a href="/<?php echo htmlentities($booklist_act); ?>">分类</a></li>
            <li><a href="/<?php echo htmlentities($update_act); ?>">更新</a></li>
            <li><a href="/<?php echo htmlentities($rank_ctrl); ?>">排行</a></li>
            <li><a href="/<?php echo htmlentities($booklist_act); ?>"><i class="icon icon-cat" style="font-size:19px;vertical-align: sub;"></i></a></li>
        </ul>

        <!-- 搜索栏 -->
        <div class="header-search">
            <input id="txtKeywords" type="text" value="">
            <a id="btnSearch">搜索</a>
            <script>
                $('#btnSearch').click(function () {
                    var keyword = $('#txtKeywords').val();
                    window.location.href = '/<?php echo htmlentities($search_ctrl); ?>?keyword=' + keyword;
                })
            </script>
        </div>
        <ul class="header-bar">
            <li class="vip">
                <a href="/vip">
                    <div class="header-vip"></div>
                    <p>VIP</p>
                </a>
            </li>
            <li class="hover">
                <a href="/history" target="_blank">
                    <i class="icon icon-clock"></i>
                    <p>历史</p>
                </a>
            </li>
            <li class="hover">
                <a href="/bookshelf" data-isload="0" target="_blank">
                    <i class="icon icon-fav"></i>
                    <p>收藏</p>
                    <span class="red-sign"></span>
                </a>
            </li>
        </ul>
        <div class="header_login hover">
            <a href="/login" class="js_header_login_btn">
                <img data-isload="0" class="header-avatar" src="/static/images/mrtx.gif">
            </a>
        </div>
    </div>
</header>

<div class="container">
    <div class="shutter">
        <div class="shutter-img">
            <?php $banners = getBanners("banner_order desc","1=1","0"); if(is_array($banners) || $banners instanceof \think\Collection || $banners instanceof \think\Paginator): $i = 0; $__LIST__ = $banners;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <a data-shutter-title="<?php echo htmlentities($vo['title']); ?>"
               href="<?php if($end_point == 'id'): ?>
              /<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['book_id']); else: ?>
              /<?php echo htmlentities($book_ctrl); ?>/<?php echo !empty($vo['book']) ? htmlentities($vo['book']['unique_id']) :  ''; ?>
              <?php endif; ?>">
                <img alt="<?php echo htmlentities($vo['title']); ?>" src="<?php echo htmlentities($vo['pic_name']); ?>">
            </a>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
        <ul class="shutter-btn">
            <li class="prev"></li>
            <li class="next"></li>
        </ul>
        <div class="shutter-desc">
            <?php $banners = getBanners("banner_order desc","1=1","0"); if(is_array($banners) || $banners instanceof \think\Collection || $banners instanceof \think\Paginator): $i = 0; $__LIST__ = $banners;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <p><?php echo htmlentities($vo['title']); ?></p>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </div>
</div>

<div class="full-class">
    <div class="container">
        <ul class="list">
            <?php $cates = getCates("id desc","1=1","0"); if(is_array($cates) || $cates instanceof \think\Collection || $cates instanceof \think\Paginator): $i = 0; $__LIST__ = $cates;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li><a href="/<?php echo htmlentities($booklist_act); ?>?cate=<?php echo htmlentities($vo['tag_name']); ?>"><?php echo htmlentities($vo['tag_name']); ?></a></li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <a class="full-class-btn" href="/<?php echo htmlentities($booklist_act); ?>/">全部分类</a>
    </div>
</div>

<script>
    $(function () {
        $('.shutter').shutter({
            shutterW: 1000, // 容器宽度
            shutterH: 358, // 容器高度
            isAutoPlay: true, // 是否自动播放
            playInterval: 3000, // 自动播放时间
            curDisplay: 3, // 当前显示页
            fullPage: false // 是否全屏展示
        });
    });
</script>
<link href="/static/css/lunbo.css" rel="stylesheet" type="text/css">
<script src="/static/js/shutter.js"></script>
<script src="/static/js/velocity.js"></script>

<div class="index-title">
    <div class="container">
        <img class="index-title-logo" src="http://css99tel.cdndm5.com/v201910141105/blue/images/sd/index-title-7.png">
        <h2>上升最快</h2>
        <a class="index-title-more" href="/<?php echo htmlentities($rank_ctrl); ?>">更多</a>
    </div>
</div>

<div class="index-manga">
    <div class="container overflow-Show">
        <ul class="mh-list col7">
            <?php $books = getBooks("last_time desc","1=1","7"); if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li>
                <div class="mh-item">
                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                        <p class="mh-cover "
                           style="background-image: url(<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>)"></p>
                    </a>

                    <div class="mh-item-detali">
                        <h2 class="title">
                            <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                <?php echo htmlentities($vo['book_name']); ?>
                            </a>
                        </h2>
                        <p class="zl"><span>评分:</span><span class="mh-star-line star-4"></span></p>
                        <p class="chapter">
                            <?php echo htmlentities($vo['summary']); ?>
                        </p>
                    </div>
                </div>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
</div>

<div class="index-title">
    <div class="container">
        <img class="index-title-logo" src="/static/images/index-title-1.png">
        <h2>热门漫画</h2>
        <a class="index-title-more" href="/<?php echo htmlentities($rank_ctrl); ?>">更多</a>
    </div>
</div>
<div class="index-original">
    <div class="container">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-slide-duplicate">
                    <ul class="index-original-list">
                        <?php $books = getBooks("hits desc","1=1","6"); if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        <li>
                            <div class="cover">
                                <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                    <p class="mh-cover " style="background-image: url(<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>)"></p>
                                </a>
                                </a>
                            </div>
                            <div class="info">
                                <p class="title">
                                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                        <?php echo htmlentities($vo['book_name']); ?>
                                    </a>

                                </p>
                                <div class="star"><span class="active"></span>
                                    <span class="active"></span>
                                    <span class="active"></span>
                                    <span class="active"></span>
                                    <span></span>
                                </div>
                                <p class="subtitle" style="color: #252525;">总点击
                                    <span>
                                         <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                            <?php echo htmlentities($vo['hits']); ?>
                                        </a>
                                    </span>
                                </p>
                                <p class="desc"><?php echo htmlentities($vo['author_name']); ?></p>
                                <div class="tag">
                                    <?php if(is_array($vo['taglist']) || $vo['taglist'] instanceof \think\Collection || $vo['taglist'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['taglist'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag): $mod = ($i % 2 );++$i;?>
                                    <a href="/<?php echo htmlentities($booklist_act); ?>/<?php echo htmlentities($tag); ?>" target="_blank"><?php echo htmlentities($tag); ?></a>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </div>
                            </div>
                        </li>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

<section class="box container overflow-Show">
    <header class="box-header"><i class="icon icon-cat"></i>
        <h2>热门分类</h2>
        <ul class="cat-tabs js-tabs" style="margin-top: 1rem;">
            <?php $cates = getCates("id desc","1=1","14"); if(is_array($cates) || $cates instanceof \think\Collection || $cates instanceof \think\Paginator): $i = 0; $__LIST__ = $cates;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?> <!-- value里的数字是后台分类ID，填哪几个数字就展示哪几个分类，必须和下面一致 -->
            <li><a class="<?php echo $i==1 ? 'active'  :  ''; ?>" data-val="<?php echo htmlentities($i); ?>" onclick="switchTag(this)"><?php echo htmlentities($vo['tag_name']); ?></a></li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <div class="pull-right">
            <a class="more" href="/<?php echo htmlentities($booklist_act); ?>?end=1">更多<i class="icon icon-arrow"></i></a>
        </div>
    </header>
    <div class="box-body">
        <section class="box-con-8n3">
            <div class="box-8n3-pane js_box_pane active">
                <?php $cates = getCates("id desc","1=1","14"); if(is_array($cates) || $cates instanceof \think\Collection || $cates instanceof \think\Paginator): $k = 0; $__LIST__ = $cates;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$cate): $mod = ($k % 2 );++$k;?>
                <ul class="mh-list col7 switch-books" data-val="<?php echo htmlentities($k); ?>" style="display: <?php echo $k==1 ? 'block'  :  'none'; ?>;">
                    <?php $books = getBooks("last_time desc","tags like '%$cate->tag_name%'","14"); if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li>
                        <div class="mh-item">
                            <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                <p class="mh-cover " style="background-image: url(<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>)"></p>
                            </a>


                            <div class="mh-item-detali">
                                <h2 class="title">
                                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                        <?php echo htmlentities($vo['book_name']); ?>
                                    </a>

                                </h2>
                                <p class="zl"><span>评分:</span><span class="mh-star-line star-4"></span></p>
                                <p class="chapter">
                                    <?php echo htmlentities($vo['summary']); ?>
                                </p>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        </section>
    </div>
    <footer></footer>
</section>
<script>
    function switchTag(obj) {
        var i = $(obj).attr('data-val');
        console.log(i);
        $('.switch-books').each(function (index, el) {
            if ($(el).attr('data-val') == i) {
                $(el).show();
            } else {
                $(el).hide();
            }
        })
        $(obj).addClass('active').parent().siblings().children('a').removeClass('active');
        console.log($(obj));
    }
</script>

<!-- 热门排行 -->
<div class="container">
    <div class="index-rank">
        <div class="index-rank-title">新书榜<a class="more" href="/<?php echo htmlentities($rank_ctrl); ?>">更多</a></div>
        <ul class="index-rank-list">
            <?php $books = getBooks("id desc","1=1","4"); if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li>
                <div class="cover">
                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                        <img alt="<?php echo htmlentities($vo['book_name']); ?>" src="<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>">
                    </a>

                </div>
                <div class="sign"><span class="top-<?php echo htmlentities($k); ?>"><?php echo htmlentities($k); ?></span></div>
                <div class="info">
                    <p class="title">
                        <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                            <?php echo htmlentities($vo['book_name']); ?>
                        </a>

                    </p>
                    <p class="subtitle"><?php echo htmlentities(subtext($vo['summary'],15)); ?></p>
                </div>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
    <div class="index-rank">
        <div class="index-rank-title">点击榜<a class="more" href="/<?php echo htmlentities($rank_ctrl); ?>">更多</a></div>
        <ul class="index-rank-list">
            <?php $books = getBooks("hits desc","1=1","4"); if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li>
                <div class="cover">
                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                        <img alt="<?php echo htmlentities($vo['book_name']); ?>" src="<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>">
                    </a>

                </div>
                <div class="sign"><span class="top-<?php echo htmlentities($k); ?>"><?php echo htmlentities($k); ?></span></div>
                <div class="info">
                    <p class="title">
                        <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                            <?php echo htmlentities($vo['book_name']); ?>
                        </a>

                    </p>
                    <p class="subtitle"><?php echo htmlentities(subtext($vo['summary'],15)); ?></p>
                </div>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
    <div class="index-rank">
        <div class="index-rank-title">推荐榜<a class="more" href="/<?php echo htmlentities($rank_ctrl); ?>">更多</a></div>
        <ul class="index-rank-list">
            <?php $books = getBooks("last_time desc","is_top=1","4"); if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li>
                <div class="cover">
                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                        <img alt="<?php echo htmlentities($vo['book_name']); ?>" src="<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>">
                    </a>

                </div>
                <div class="sign"><span class="top-<?php echo htmlentities($k); ?>"><?php echo htmlentities($k); ?></span></div>
                <div class="info">
                    <p class="title">
                        <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                            <?php echo htmlentities($vo['book_name']); ?>
                        </a>


                    </p>
                    <p class="subtitle"><?php echo htmlentities(subtext($vo['summary'],15)); ?></p>
                </div>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
</div>


<section class="box container overflow-Show">
    <header class="box-header"><i class="icon icon-end"></i>
        <h2>完结优选</h2>
        <div class="pull-right">
            <a class="more" href="/<?php echo htmlentities($booklist_act); ?>?end=1">更多<i class="icon icon-arrow"></i></a></div>
    </header>
    <div class="box-body">
        <section class="box-con-8n3">
            <div class="box-8n3-pane js_box_pane active">
                <ul class="mh-list col7">
                    <?php $books = getBooks("id desc","end=1","14"); if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li>
                        <div class="mh-item">
                            <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                <p class="mh-cover " style="background-image: url(<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>)"></p>
                            </a>


                            <div class="mh-item-detali">
                                <h2 class="title">
                                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>">
                                        <?php echo htmlentities($vo['book_name']); ?>
                                    </a>
                                </h2>
                                <p class="zl"><span>评分:</span><span class="mh-star-line star-4"></span></p>
                                <p class="chapter">
                                    <?php echo htmlentities($vo['summary']); ?>
                                </p>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </section>
    </div>
    <footer></footer>
</section>

<div class="index-friendship">
    <div class="container">
        <p class="title">友情链接</p>
        <ul class="index-friendship-list">
            <?php if(is_array($links) || $links instanceof \think\Collection || $links instanceof \think\Paginator): $i = 0; $__LIST__ = $links;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li><a href="<?php echo htmlentities($vo['url']); ?>" target="_blank" title="<?php echo htmlentities($vo['name']); ?>"><?php echo htmlentities($vo['name']); ?></a></li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
</div>
<ul class="index-right-float" style="display: block;">
    <li><a class="active"
           href="javascript:void(0);"
           onclick="$('html,body').stop().animate({ scrollTop:$('.box-header').eq(0).offset().top-80},500);">最近<br>更新</a>
    </li>
    <li><a class=""
           href="javascript:void(0);"
           onclick="$('html,body').stop().animate({ scrollTop:$('.index-title').eq(0).offset().top-80},500);">热门<br>漫画</a>
    </li>
    <li><a class=""
           href="javascript:void(0);"
           onclick="$('html,body').stop().animate({ scrollTop:$('.index-rank').eq(0).offset().top-80},500);">排行<br>榜单</a>
    </li>
    <li><a href="javascript:void(0);"
           onclick="$('html,body').stop().animate({ scrollTop:$('.box-header').eq(1).offset().top-80},500);">完结<br>佳作</a>
    </li>
    <li><a class="index-right-float-top" href="javascript:void(0);"
           onclick="$('html,body').stop().animate({ scrollTop:0},500);"></a></li>
</ul>
<script type="text/javascript">
    $(function () {
        $(window).scroll(function () {
            var scrollTop = (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) + 5;
            if (scrollTop < $('.box-header').eq(0).offset().top - 80) {
                $('.index-right-float li a').removeClass('active');
                $('.index-right-float li a').eq(0).addClass('active');
            } else if (scrollTop < $('.index-title').eq(0).offset().top - 80) {
                $('.index-right-float li a').removeClass('active');
                $('.index-right-float li a').eq(1).addClass('active');
            } else if (scrollTop < $('.index-rank').eq(0).offset().top - 80) {
                $('.index-right-float li a').removeClass('active');
                $('.index-right-float li a').eq(2).addClass('active');
            } else if (scrollTop < $('.box-header').eq(1).offset().top - 80) {
                $('.index-right-float li a').removeClass('active');
                $('.index-right-float li a').eq(5).addClass('active');
            } else {
                $('.index-right-float li a').removeClass('active');
                $('.index-right-float li a').eq(6).addClass('active');
            }
        });
    });
</script>

<div class="footer mt16 cf">
    <div class="footer-wrap">
        <div class="footer-cont container">
            <p><?php echo htmlentities($site_name); ?>为纯属个人爱好从互联网中收集。并不具备版权，也没存储任何图片信息,所以免费分享供朋友们阅读，朋友们如果喜欢，且有支付能力，请您一定支持正版.本站收集纯属无奈，中国有太多的人真的没有支付能力。我希望我们若干年后，拥有了支付能力，一定像我去看周星驰的大话西游一样。还他一张票钱。
            </p>
            <p>Copyright © 2020-2022 <em>/</em> <?php echo htmlentities($url); ?> <em>/</em> All Rights Reserved <em>/</em>
                <a href="#" target="_blank" rel="nofollow">版权投诉</a><a href="/sitemap.html" target="_blank" rel="nofollow">站点地图</a> 联系我们
            </p>
        </div>
    </div>
</div>
<div class="alertTop_1" style="display: none;">
    <p id="alertTop_1"></p>
</div>
<script src="/static/js/user-booklist.js"></script>
<!--自动提交-->
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?4f2c9b6c4700a255e81f0d2c017c664d";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
    (function () {
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        } else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
</script>

</body>

</html>