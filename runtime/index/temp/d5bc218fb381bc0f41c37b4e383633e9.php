<?php /*a:3:{s:39:"./template/default/pc/update/index.html";i:1669086406;s:35:"./template/default/pc/pub/base.html";i:1669263232;s:37:"./template/default/pc/pub/others.html";i:1665224102;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="baidu-site-verification" content="code-QrWB2ibm17" />
    <meta name="shenma-site-verification" content="0a1dd4a9539d78a516530b63b949795b_1669263148">
    <link rel="shortcut icon" href="/static/images/favicon.ico">
    <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon.ico">
    <meta name="applicable-device" content="pc,mobile">
    <meta name="Keywords" content="免费国漫,免费韩漫,免费日漫,免费漫画,聚合漫画,<?php echo htmlentities($site_name); ?>">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo htmlentities($current_url); ?>"/>
    <meta property="og:site_name" content="<?php echo htmlentities($site_name); ?>"/>
    <meta property="og:image" content="/static/images/favicon.ico"/>
    <script type="application/ld+json">{"@context": "https://schema.org","@type": "Organization","url": "<?php echo htmlentities($current_url); ?>","logo": "https://www.52hah.com/static/images/favicon.ico"}</script>


    
<title>最新免费漫画大全-<?php echo htmlentities($site_name); ?></title>
<meta property="og:title" content="最新免费漫画大全-<?php echo htmlentities($site_name); ?>"/>

<meta property="og:description" content= "<?php echo htmlentities($site_name); ?>为大家带来最新最全免费漫画大全，这里有超多漫画资源，无论是国产动漫 韩国漫画，耽美漫画，还是恐怖漫画全都免费看，总有一个让你酣畅淋漓" />
<meta content="<?php echo htmlentities($site_name); ?>为大家带来最新最全免费漫画大全，这里有超多漫画资源，无论是国产动漫 韩国漫画，耽美漫画，还是恐怖漫画全都免费看，总有一个让你酣畅淋漓" name="description">


    <link href="/static/css/style.css" rel="stylesheet" type="text/css">
<!--    <link href="/static/css/userinfo-vendor.css" rel="stylesheet" type="text/css">-->

    <link href="/static/css/dm5_style.css" rel="stylesheet" type="text/css">
    
    <script src="https://lib.baomitu.com/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://lib.baomitu.com/vue/2.6.10/vue.min.js"></script>
    <script src="https://lib.baomitu.com/axios/0.19.2/axios.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery_lazyload/1.9.7/jquery.lazyload.min.js"></script>
    <script src="/static/js/index.js"></script>

</head>

<body class="white">
<!-- 页面头部 -->
<header class="header container-fluid " style="left: 0px;">
    <div class="container">
        <!-- 左侧logo -->
        <a href="/">
            <!--            <p>聚合漫画屋</p>-->
            <img class="header-logo" alt="<?php echo htmlentities($site_name); ?>" src="/static/images/header-logo.png">
        </a>
        <!-- 左侧菜单标题 -->
        <ul class="header-title">
            <li><a href="/">首页</a></li>
            <li><a href="/<?php echo htmlentities($booklist_act); ?>">分类</a></li>
            <li><a href="/<?php echo htmlentities($update_act); ?>">更新</a></li>
            <li><a href="/<?php echo htmlentities($rank_ctrl); ?>">排行</a></li>
            <li><a href="/<?php echo htmlentities($booklist_act); ?>"><i class="icon icon-cat" style="font-size:19px;vertical-align: sub;"></i></a></li>
        </ul>

        <!-- 搜索栏 -->
        <div class="header-search">
            <input id="txtKeywords" type="text" value="">
            <a id="btnSearch">搜索</a>
            <script>
                $('#btnSearch').click(function () {
                    var keyword = $('#txtKeywords').val();
                    window.location.href = '/<?php echo htmlentities($search_ctrl); ?>?keyword=' + keyword;
                })
            </script>
        </div>
        <ul class="header-bar">
            <li class="vip">
                <a href="/vip">
                    <div class="header-vip"></div>
                    <p>VIP</p>
                </a>
            </li>
            <li class="hover">
                <a href="/history" target="_blank">
                    <i class="icon icon-clock"></i>
                    <p>历史</p>
                </a>
            </li>
            <li class="hover">
                <a href="/bookshelf" data-isload="0" target="_blank">
                    <i class="icon icon-fav"></i>
                    <p>收藏</p>
                    <span class="red-sign"></span>
                </a>
            </li>
        </ul>
        <div class="header_login hover">
            <a href="/login" class="js_header_login_btn">
                <img data-isload="0" class="header-avatar" src="/static/images/mrtx.gif">
            </a>
        </div>
    </div>
</header>

<section class="dateProgressBar container-fluid">
    <div class="bar-wrap container-fluid">
        <ul class="data-list container">
            <?php $__FOR_START_1572023117__=0;$__FOR_END_1572023117__=7;for($i=$__FOR_START_1572023117__;$i < $__FOR_END_1572023117__;$i+=1){ ?>
            <li class="lidaykey <?php echo $day==$i ? 'active'  :  ''; ?>">
                <a class="bt_daykey" data-val="<?php echo htmlentities($i); ?>">
                    <span class="day"></span>
                    <span class="date"></span>
                </a>
            </li>
            <?php } ?>
        </ul>
    </div>
    <script src="https://lib.baomitu.com/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://lib.baomitu.com/moment.js/2.24.0/locale/zh-cn.js"></script>
    <script>
        $('.bt_daykey').each(function () {
            var day = $(this).attr('data-val');
            $(this).children('.day').html(moment().subtract(day,'days').format('dddd').replace('星期','周'));
            $(this).children('.date').html(moment().subtract(day,'days').format('MMMDo').replace('星期','周'));
            $(this).click(function () {
                var date = moment().subtract(day,'days').format('YYYY-MM-DD');
                location.href = '/<?php echo htmlentities($update_act); ?>?date=' + date + '&day=' + day;
            })
        })
    </script>
</section>
<section class="box container pb40 overflow-Show js_update_mh_list" style="min-height:800px;margin-top: 20px;">
    <header class="box-header"> <i class="icon icon-date" style="width:30px;"> <span class="number">30</span> </i>
        <h1>最近更新</h1>
        <small>
            更新了
            <span class="color-main" id="daykeyupdatecount">若干</span><span id="daykeycontent">本，持续更新中</span>
        </small>
    </header>
    <div class="box-body">
        <ul class="mh-list col7" id="update_30">
            <?php $data = getPagedBooks("last_time desc","last_time>=$start_time and last_time<=$end_time","28");$page = $data["page"];$param = $data["param"];$books = $data["books"]; if(is_array($books) || $books instanceof \think\Collection || $books instanceof \think\Paginator): $i = 0; $__LIST__ = $books;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li>
                <div class="mh-item">
                    <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>" title="<?php echo htmlentities($vo['book_name']); ?>">
                        <p class="mh-cover"  style="background-image: url(<?php echo htmlentities((get_img_url($vo['cover_url']) ?: '')); ?>)"></p>
                    </a>
                    <div class="mh-item-detali">
                        <h2 class="title">
                            <a href="/<?php echo htmlentities($book_ctrl); ?>/<?php echo htmlentities($vo['param']); ?>" title="<?php echo htmlentities($vo['book_name']); ?>" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                <?php echo htmlentities($vo['book_name']); ?>
                            </a>
                        </h2>
                        <p class="zl">
                            <span>评分:</span>
                            <span class="mh-star-line star-5"></span>
                        </p>
                        <p class="chapter" style="width: 181px;">
                            <?php echo htmlentities($vo['summary']); ?>
                        </p>
                    </div>
                </div>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
    <footer>
        <div class="page-pagination mt20">
            <ul>
                <div class="pagination">
                    <?php if($page['last_page'] > 1): if($page['current_page'] == 1): ?>
                    <li><a>&lt;</a></li>
                    <?php else: ?>
                    <li><a id="prevPage" href="/<?php echo htmlentities($update_act); ?>?page=<?php echo htmlentities($page['current_page']-1); ?><?php echo htmlentities($param); ?>" title="上一页">&lt;</a></li>
                    <?php endif; $__FOR_START_1082134047__=1;$__FOR_END_1082134047__=$page['last_page'] + 1;for($i=$__FOR_START_1082134047__;$i < $__FOR_END_1082134047__;$i+=1){ if($i == $page['current_page']): ?>
                    <li><a href="/<?php echo htmlentities($update_act); ?>?page=<?php echo htmlentities($i); ?><?php echo htmlentities($param); ?>" class="active"><?php echo htmlentities($i); ?></a></li>
                    <?php elseif(($i > $page['current_page'] - 4 && $i < $page['current_page'] + 5)): ?>
                    <li><a href="/<?php echo htmlentities($update_act); ?>?page=<?php echo htmlentities($i); ?><?php echo htmlentities($param); ?>" title="第<?php echo htmlentities($i); ?>页"><?php echo htmlentities($i); ?></a></li>
                    <?php endif; } if($page['current_page'] == $page['last_page']): ?>
                    <li><a>&gt;</a></li>
                    <?php else: ?>
                    <li><a id="nextPage" href="/<?php echo htmlentities($update_act); ?>?page=<?php echo htmlentities($page['current_page']+1); ?><?php echo htmlentities($param); ?>" title="下一页">&gt;</a></li>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </ul>
        </div>
    </footer>
</section>

<div class="footer mt16 cf">
    <div class="footer-wrap">
        <div class="footer-cont container">
            <p><?php echo htmlentities($site_name); ?>为纯属个人爱好从互联网中收集。并不具备版权，也没存储任何图片信息,所以免费分享供朋友们阅读，朋友们如果喜欢，且有支付能力，请您一定支持正版.本站收集纯属无奈，中国有太多的人真的没有支付能力。我希望我们若干年后，拥有了支付能力，一定像我去看周星驰的大话西游一样。还他一张票钱。
            </p>
            <p>Copyright © 2020-2022 <em>/</em> <?php echo htmlentities($url); ?> <em>/</em> All Rights Reserved <em>/</em>
                <a href="#" target="_blank" rel="nofollow">版权投诉</a><a href="/sitemap.html" target="_blank" rel="nofollow">站点地图</a> 联系我们
            </p>
        </div>
    </div>
</div>
<div class="alertTop_1" style="display: none;">
    <p id="alertTop_1"></p>
</div>
<script src="/static/js/user-booklist.js"></script>
<!--自动提交-->
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?4f2c9b6c4700a255e81f0d2c017c664d";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
    (function () {
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        } else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
</script>

</body>

</html>