<?php


namespace app\index\controller;

use think\facade\View;

class Update extends Base
{
    public function index()
    {
        $date = input('date');
        $day = input('day');
        if (empty($date)) {
            $time = strtotime(date('Y-m-d'));
            $start_time = strtotime("-7 day");
            $end_time = strtotime(date('Y-m-d 23:55:s'));
        } else {
            $time = strtotime($date);
            $start_time = $time;
            $end_time = $time+60*60*24;
        }


        View::assign([
            'day' => $day == null ? -1 : $day,
            'time' => $time,
            'start_time'=>$start_time,
            'end_time'=>$end_time
        ]);
        return view($this->tpl);
    }


}