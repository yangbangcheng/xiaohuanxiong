<?php


namespace app\app\controller;


use app\app\rewrite\Captcha;
use app\Request;
use app\service\PromotionService;
use app\validate\Phone;
use app\validate\User as UserValidate;
use app\model\User;
use think\Config;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use Firebase\JWT\JWT;
use think\facade\Session;

class Account extends Base
{

    public function register()
    {
        $data = request()->param();
        $captcha = trim(input('captcha'));
        if( !$this->captcha->check($captcha))
        {
            return json(['success' => 0, 'msg' => '验证码错误']);
        }
        $validate = new UserValidate();
        if ($validate->check($data)) {
            try {
                $user = User::where('username', '=', trim(request()->param('username')))->findOrFail();
                return json(['success' => 0, 'msg' => '用户名已存在']);
            } catch (ModelNotFoundException $e) {
                if(!empty(request()->param('email'))){
                    $email_exists = User::where('email', '=', trim(request()->param('email')))->find();
                    if($email_exists){
                        return ['err' => 1, 'msg' => '邮箱已被注册'];
                    }
                }
                $user = new User();
                $user->username = trim(request()->param('username'));
                $user->password = trim(request()->param('password'));
                $user->suid = gen_uid(24);
                $user->nick_name = trim(request()->param('nick_name'));

                $user->email = trim(request()->param('email'));
                $user->level = 2;
                $result = $user->save();
                if ($result) {
                    return json(['success' => 1, 'msg' => '注册成功，请登录']);
                } else {
                    return json(['success' => 0, 'msg' => '注册失败，请尝试重新注册']);
                }
            }
        } else {
            return json(['success' => 0, 'msg' => $validate->getError()]);
        }
    }

    public function phonereg()
    {
        $data = request()->param();
        if (vcode($data['stoken'], $data['code'], $data['mobile']) == 0) {
            return json(['success' => 0, 'msg' => '验证码错误']);
        }
        $validate = new Phone();
        if ($validate->check($data)) {
            try {
                return json(['success' => 0, 'msg' => '手机号已存在，请直接登录']);
            } catch (ModelNotFoundException $e) {
                $user = new User();
                $user->username = gen_uid(12);
                $user->password = 'abc123';
                $user->suid = gen_uid(24);
                $user->level = 2;
                $user->mobile = trim($data['mobile']);
                $result = $user->save();
                if ($result) {
                    return json(['success' => 1, 'msg' => '注册成功，请登录']);
                } else {
                    return json(['success' => 0, 'msg' => '注册失败，请尝试重新注册']);
                }
            }
        }
    }

    public function login()
    {
        $map = array();
        $map[] = ['username', '=', trim(input('username'))];
        $map[] = ['password', '=', md5(strtolower(trim(input('password'))) . config('site.salt'))];
        try {
            $user = User::withTrashed()->where($map)->findOrFail();
            if ($user->delete_time > 0) {
                return json(['success' => 0, 'msg' => '用户被锁定']);
            } else {
                $financeService = app('financeModel');
                $user['balance'] = $financeService->getBalance($user->id); //获取用户余额
                $key = config('site.api_key');
                $token = [
                    "iat" => time(), //签发时间
                    "nbf" => time(), //在什么时候jwt开始生效  （这里表示生成100秒后才生效）
                    "exp" => time() + 60 * 60 * 24, //token 过期时间
                    "uid" => $user->id, //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
                    "suid" => $user->suid,
                    "level" => $user->level
                ];
                $this->uid = $user->id;
                $utoken = JWT::encode($token, $key, "HS256");
                $user['utoken'] = $utoken;
                return json(['success' => 1, 'userInfo' => $user]);
            }
        } catch (DataNotFoundException $e) {
            return json(['success' => 0, 'msg' => '用户名或密码错误']);
        } catch (ModelNotFoundException $e) {
            return json(['success' => 0, 'msg' => '用户名或密码错误']);
        }
    }

    public function autologin()
    {
        $data = request()->param();
        $map = array();
        $map[] = ['suid', '=', trim($data['suid'])];
        $key = config('site.api_key');
        try {
            $user = User::withTrashed()->where($map)->findOrFail();
            if ($user->delete_time > 0) {
                return json(['success' => 0, 'msg' => '用户被锁定']);
            } else {
                $this->uid = $user->id;
                $financeService = app('financeModel');
                $user['balance'] = $financeService->getBalance($user->id); //获取用户余额
                $token = [
                    "iat" => time(), //签发时间
                    "nbf" => time(), //在什么时候jwt开始生效  （这里表示生成100秒后才生效）
                    "exp" => time() + 60 * 60 * 24, //token 过期时间
                    "uid" => $user->id, //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
                    "suid" => $user->suid,
                    "level" => $user->level
                ];
                $utoken = JWT::encode($token, $key, "HS256");
                $user['utoken'] = $utoken;
                return json(['success' => 1, 'userInfo' => $user]);
            }
        } catch (ModelNotFoundException $e) {
            $user = new User();
            $user->username = gen_uid(12);
            $user->password = 'abc123';
            $user->suid = $data['suid'];
            $user->level = 1; //游客
            $user->mobile = '';
            $result = $user->save();
            if ($result) {
                $token = [
                    "iat" => time(), //签发时间
                    "nbf" => time(), //在什么时候jwt开始生效  （这里表示生成100秒后才生效）
                    "exp" => time() + 60 * 60 * 24, //token 过期时间
                    "uid" => $user->id, //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
                    "suid" => $user->suid,
                    "level" => $user->level
                ];
                $utoken = JWT::encode($token, $key, "HS256");
                $user['utoken'] = $utoken;
                $user['balance'] = 0;
                return json(['success' => 1, 'userInfo' => $user]);
            } else {
                return json(['success' => 0, 'msg' => '登录失败，请尝试重新登录']);
            }
        } catch (DataNotFoundException $e) {
            $user = new User();
            $user->username = gen_uid(12);
            $user->password = 'abc123';
            $user->suid = $data['suid'];
            $user->level = 1; //游客
            $user->mobile = '';
            $result = $user->save();
            if ($result) {
                $token = [
                    "iat" => time(), //签发时间
                    "nbf" => time(), //在什么时候jwt开始生效  （这里表示生成100秒后才生效）
                    "exp" => time() + 60 * 60 * 24, //token 过期时间
                    "uid" => $user->id, //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
                    "suid" => $user->suid,
                    "level" => $user->level
                ];
                $utoken = JWT::encode($token, $key, "HS256");
                $user['utoken'] = $utoken;
                $user['balance'] = 0;
                return json(['success' => 1, 'userInfo' => $user]);
            } else {
                return json(['success' => 0, 'msg' => '登录失败，请尝试重新登录']);
            }
        }
    }

    public function phonelogin()
    {
        $data = request()->param();
        if (verifycode($data['stoken'], $data['code'], $data['mobile']) == 0) {
            return json(['success' => 0, 'msg' => '验证码错误']);
        }
        $validate = new Phone();
        if ($validate->check($data)) {
            $key = config('site.api_key');
            try {
                $user = User::withTrashed()->where('mobile', '=', trim($data['mobile']))->findOrFail();
                if ($user->delete_time > 0) {
                    return json(['success' => 0, 'msg' => '用户被锁定']);
                } else {
                    $this->uid = $user->id;
                    $financeService = app('financeModel');
                    $user['balance'] = $financeService->getBalance($user->id); //获取用户余额
                    $token = [
                        "iat" => time(), //签发时间
                        "nbf" => time(), //在什么时候jwt开始生效  （这里表示生成100秒后才生效）
                        "exp" => time() + 60 * 60 * 24 * 15, //token 过期时间
                        "uid" => $user->id, //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
                        "suid" => $user->suid,
                        "level" => $user->level,
                    ];
                    $utoken = JWT::encode($token, $key, "HS256");
                    $user['utoken'] = $utoken;
                    return json(['success' => 1, 'userInfo' => $user]);
                }
            } catch (ModelNotFoundException $e) {
                return json(['success' => 0, 'msg' => '手机号不存在']);
            }
        } else {
            return json(['success' => 0, 'msg' => $validate->getError()]);
        }
    }

    public function checkAuth()
    {
        $utoken = input('utoken');
        if (isset($utoken)) {
            $json = $this->getAuth($utoken);
        } else {
            $json = json(['success' => 0, 'msg' => '传递参数错误']);
        }
        return $json;
    }

    public function logout()
    {
        $utoken = input('utoken');
        $key = config('site.api_key');
        $info = JWT::decode($utoken, $key, array('HS256', 'HS384', 'HS512', 'RS256'));
        $arr = (array)$info;
        if ($this->uid == $arr['suid']) {
            unset($this->uid);
        }
        return json(['success' => 1, 'msg' => '登出成功']);
    }

    public function resetpwd()
    {
        $pwd = trim(input('password'));
        $username = trim(input('username'));
        $email = trim(input('email'));
        $captcha = trim(input('captcha'));

        if( !$this->captcha->check($captcha))
        {
            return json(['success' => 0, 'msg' => '验证码错误']);
        }
        $validate = new \think\Validate;
        $validate->rule('username', 'require');
        $validate->rule('password', 'require|min:6|max:21');
        $validate->rule('email', 'require|email');

        $validate->message([
            'username.require' => '用户名必填',
            'password.min' => '密码最小6位',
            'password.max' => '密码不能超过21位',
            'email.require' => '邮箱必填',
            'email.email' => '邮箱格式错误',
        ]);
        $data = [
            'password' => $pwd,
            'email' => $email,
            'username' => $username
        ];

        if (!$validate->check($data)) {
            return json(['msg' => $validate->getError(), 'success' => 0]);
        }
        try {
            $user = User::where(['username' => $username, 'email' => $email])->findOrFail();
            $user->password = $pwd;
            $user->save();
            return json(['msg' => '修改成功', 'success' => 1]);
        } catch (DataNotFoundException $e) {
            return json(['success' => 0, 'msg' => '用户不存在']);
        } catch (ModelNotFoundException $e) {
            return json(['success' => 0, 'msg' => '用户不存在']);
        }
    }
    public function captcha()
    {
        ob_clean();

        return $this->captcha->create();

    }
}