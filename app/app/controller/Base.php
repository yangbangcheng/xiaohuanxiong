<?php


namespace app\app\controller;


use app\app\rewrite\Captcha;
use app\BaseController;
use app\constants\HttpCode;
use think\facade\Env;
use Firebase\JWT\JWT;
use think\facade\Session;

class Base extends BaseController
{
    public $prefix;
    public $url;
    public $img_domain;
    public $book_ctrl;
    public $uid;
    public $vip_expire_time;
    protected $end_point;
    public $captcha;


    public function getAuth($utoken)
    {
        $key = config('site.api_key');
        try {
            $info = JWT::decode($utoken, $key, array('HS256', 'HS384', 'HS512', 'RS256'));
            $arr = (array)$info;
            return json(['success' => 1, 'userInfo' => $arr]);
        } catch (\Exception $e) {
            return json(['success' => -1, 'msg' => $e->getMessage(), HttpCode::ERR_NOT_AUTH]);
        }
    }

    protected function initialize()
    {
        parent::initialize();

        isset($_SERVER['HTTP_ORIGIN']) ? header('Access-Control-Allow-Origin:' . $_SERVER['HTTP_ORIGIN'])
            : header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods:GET, POST, OPTIONS, DELETE");
        header("Access-Control-Allow-Headers:DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type, Accept-Language, Origin, Accept-Encoding");
        header("Access-Control-Allow-Credentials:true");
        $this->captcha=new Captcha(\think\facade\Config::instance(),Session::instance());

        $this->prefix = Env::get('database.prefix');
        $this->url = config('site.domain');
        $this->img_domain = config('site.img_domain');
        $this->book_ctrl = BOOKCTRL;

        $filter_route=[
            "account/captcha"
        ];
        $route= $this->request->pathinfo();
        if(in_array($route,$filter_route)){
            return;
        }

        $token = $this->request->header('token') ? $this->request->header('token') : $this->request->param('token');
        $time = $this->request->header('time') ? $this->request->header('time') : $this->request->param('time');
        if (time() - $time > 180000) {
            return json(['success' => 0, 'msg' => '过期请求'])->send();
        }
        $key = config('site.app_key');
        if ($token != md5($key . $time)) {
            return json(['success' => 0, 'msg' => '未授权请求'])->send();
        }
    }
}