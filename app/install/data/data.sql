-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}admin`;
CREATE TABLE `{{$pk}}admin`
(
    `id`              int(10) unsigned NOT NULL AUTO_INCREMENT,
    `username`        char(32) NOT NULL,
    `password`        char(32) NOT NULL,
    `create_time`     int(10) unsigned DEFAULT '0',
    `update_time`     int(10) unsigned DEFAULT '0',
    `last_login_time` int(10) unsigned DEFAULT '0',
    `last_login_ip`   varchar(100) DEFAULT '',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for {{$pk}}user
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}user`;
CREATE TABLE `{{$pk}}user`
(
    `id`              int(10) unsigned NOT NULL AUTO_INCREMENT,
    `username`        char(32) NOT NULL,
    `suid`            char(32) NOT NULL,
    `nick_name`       varchar(100) DEFAULT '',
    `mobile`          char(11)     DEFAULT '' COMMENT '会员手机号',
    `email`           char(32)     DEFAULT '' COMMENT '会员邮箱',
    `password`        char(32) NOT NULL,
    `level`           int          default '1' COMMENT '1为游客，2为正式用户,3为作者',
    `autopay`         tinyint(4) default 1 COMMENT '是否自动购买章节',
    `create_time`     int(10) unsigned DEFAULT '0',
    `update_time`     int(10) unsigned DEFAULT '0',
    `delete_time`     int(10) unsigned DEFAULT '0',
    `last_login_time` int(10) unsigned DEFAULT '0',
    `vip_expire_time` int(10) unsigned DEFAULT '0' COMMENT '会员到期时间',
    `pid`             int(10) unsigned DEFAULT '0' COMMENT '上线用户ID',
    `reg_ip`          varchar(32)  DEFAULT '' COMMENT '用户注册ip',
    PRIMARY KEY (`id`) USING BTREE,
    unique key `username` (`username`),
    key               `suid` (`suid`),
    key               `mobile` (`mobile`),
    key               `email` (`email`),
    key               `pid` (`pid`) USING BTREE
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for {{$pk}}user_finance
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}user_finance`;
CREATE TABLE `{{$pk}}user_finance`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(10) NOT NULL DEFAULT 0,
    `money`       decimal(10, 2) NOT NULL DEFAULT 0 COMMENT '充值/消费金额',
    `usage`       tinyint(4) NOT NULL COMMENT '用途，1.充值，2.购买vip，3.购买章节，4.推广奖励, 5.每日签到奖励, 6.提现',
    `summary`     text COMMENT '备注',
    `create_time` date DEFAULT NULL,
    `update_time` date DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    key           `user_id` (`user_id`) USING BTREE,
    key           `usage` (`usage`) USING BTREE,
    key `create_time` (`create_time`) USING BTREE
) ENGINE = InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for {{$pk}}user_order
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}user_order`;
CREATE TABLE `{{$pk}}user_order`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(10) NOT NULL DEFAULT 0,
    `money`       decimal(10, 2) NOT NULL DEFAULT 0 COMMENT '充值金额',
    `status`      tinyint(4) not null default 0 COMMENT '0为未支付，1为已支付',
    `pay_type`    tinyint(4) default 1 COMMENT '0为未知，1为充值金币，2为购买vip，3为提现',
    `summary`     text COMMENT '备注',
    `order_id`    varchar(100) default '' COMMENT '云端订单号',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    `expire_time` int(10) unsigned default '0',
    PRIMARY KEY (`id`) USING BTREE,
    key           `user_id` (`user_id`) USING BTREE,
    key           `status` (`status`) USING BTREE,
    key           `pay_type` (`pay_type`) USING BTREE
) ENGINE = InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for {{$pk}}user_buy
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}user_buy`;
CREATE TABLE `{{$pk}}user_buy`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(10) NOT NULL DEFAULT 0 COMMENT '购买用户ID',
    `chapter_id`  int(10) unsigned NOT NULL DEFAULT 0 COMMENT '购买漫画ID',
    `book_id`     int(10) unsigned NOT NULL DEFAULT 0 COMMENT '购买章节ID',
    `money`       decimal(10, 2) NOT NULL DEFAULT 0 COMMENT '消费金额',
    `summary`     text COMMENT '备注',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for author
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}author`;
CREATE TABLE `{{$pk}}author`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `username`    char(32)  DEFAULT 'nil',
    `password`    char(32)  DEFAULT 'nil',
    `email`       char(100) DEFAULT 'nil',
    `author_name` varchar(100) NOT NULL,
    `status`      tinyint(4) DEFAULT 0,
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    key           `username` (`username`) USING BTREE,
    key           `password` (`password`) USING BTREE,
    key           `email` (`email`) USING BTREE,
    key           `status` (`status`) USING BTREE,
    key           `author_name` (`author_name`) USING BTREE
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}banner`;
CREATE TABLE `{{$pk}}banner`
(
    `id`           int(10) unsigned NOT NULL AUTO_INCREMENT,
    `pic_name`     varchar(255) DEFAULT '' COMMENT '轮播图完整路径名',
    `create_time`  int(10) unsigned DEFAULT '0',
    `update_time`  int(10) unsigned DEFAULT '0',
    `book_id`      int(10) unsigned NOT NULL COMMENT '所属漫画ID',
    `title`        varchar(50) NOT NULL COMMENT '轮播图标题',
    `banner_order` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`id`) USING BTREE,
    KEY            `banner_order` (`banner_order`) USING BTREE
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}book`;
CREATE TABLE `{{$pk}}book`
(
    `id`              int(10) unsigned NOT NULL AUTO_INCREMENT,
    `unique_id`       char(100)   NOT NULL COMMENT '漫画标识',
    `book_name`       varchar(50) NOT NULL COMMENT '漫画名',
    `nick_name`       varchar(100)   DEFAULT '' COMMENT '别名',
    `last_chapter_id` int(10) unsigned DEFAULT '0',
    `last_chapter`    varchar(255)   DEFAULT '无章节',
    `chapter_count` int(10) unsigned DEFAULT '0',
    `create_time`     int(10) unsigned DEFAULT '0',
    `update_time`     int(10) unsigned DEFAULT '0',
    `last_time`       int(10) unsigned DEFAULT '0' COMMENT '最后更新时间',
    `delete_time`     int(10) unsigned DEFAULT '0',
    `tags`            varchar(100)   DEFAULT '' COMMENT '分类',
    `summary`         text COMMENT '简介',
    `end`             tinyint(4) DEFAULT '1' COMMENT '2为连载，1为完结',
    `author_id`       int(10) unsigned NOT NULL COMMENT '作者ID',
    `author_name`     varchar(100)   DEFAULT '佚名',
    `cover_url`       varchar(255)   DEFAULT '' COMMENT '封面图路径',
    `banner_url`      varchar(255)   DEFAULT '' COMMENT '封面横图路径',
    `start_pay`       int(10) NOT NULL DEFAULT '99999' COMMENT '第m话开始需要付费',
    `money`           decimal(10, 2) DEFAULT '0' COMMENT '每章所需费用',
    `area_id`         int(10) unsigned NOT NULL COMMENT '漫画所属地区',
    `is_top`          tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否推荐',
    `src_url`         varchar(255)   DEFAULT NULL COMMENT '原地址',
    `is_copyright`    tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否开启版权',
    `hits` int(10) unsigned DEFAULT '0' COMMENT '总人气',
    `mhits` int(10) unsigned DEFAULT '0' COMMENT '月人气',
    `whits` int(10) unsigned DEFAULT '0' COMMENT '周人气',
    `dhits` int(10) unsigned DEFAULT '0' COMMENT '日人气',
    PRIMARY KEY (`id`) USING BTREE,
    KEY               `tags` (`tags`) USING BTREE,
    KEY               `end` (`end`) USING BTREE,
    KEY               `author_id` (`author_id`) USING BTREE,
    KEY               `is_top` (`is_top`) USING BTREE,
    KEY               `area_id` (`area_id`) USING BTREE,
    KEY               `is_copyright` (`is_copyright`) USING BTREE,
    KEY               `book_name`(`book_name`) USING BTREE,
    KEY               `hits`(`hits`) USING BTREE,
    KEY               `mhits`(`mhits`) USING BTREE,
    KEY               `whits`(`whits`) USING BTREE,
    KEY               `dhits`(`dhits`) USING BTREE,
    unique KEY `unique_id`(`unique_id`)
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for chapter
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}chapter`;
CREATE TABLE `{{$pk}}chapter`
(
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `chapter_name`  varchar(255)   NOT NULL COMMENT '章节名称',
    `create_time`   int(10) unsigned DEFAULT '0',
    `update_time`   int(10) unsigned DEFAULT '0',
    `book_id`       int(10) unsigned NOT NULL COMMENT '章节所属漫画ID',
    `chapter_order` decimal(10, 2) NOT NULL COMMENT '章节序',
    PRIMARY KEY (`id`) USING BTREE,
    KEY             `chapter_name` (`chapter_name`) USING BTREE,
    KEY             `book_id` (`book_id`) USING BTREE,
    KEY             `chapter_order` (`chapter_order`) USING BTREE
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for photo
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}photo`;
CREATE TABLE `{{$pk}}photo`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `chapter_id`  int(10) unsigned NOT NULL,
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    `pic_order`   decimal(10, 2) NOT NULL COMMENT '图片序',
    `img_url`     varchar(255) DEFAULT '' COMMENT '图片路径',
    PRIMARY KEY (`id`) USING BTREE,
    KEY           `chapter_id` (`chapter_id`) USING BTREE,
    KEY           `pic_order` (`pic_order`) USING BTREE
) ENGINE=InnoDB

PARTITION BY RANGE COLUMNS(id) (
    PARTITION p0 VALUES LESS THAN (1000000),
    PARTITION p1 VALUES LESS THAN (2000000),
    PARTITION p2 VALUES LESS THAN (3000000),
    PARTITION p3 VALUES LESS THAN (4000000),
    PARTITION p4 VALUES LESS THAN (5000000),
    PARTITION p5 VALUES LESS THAN (6000000),
    PARTITION p6 VALUES LESS THAN (7000000),
    PARTITION p7 VALUES LESS THAN (8000000),
    PARTITION p8 VALUES LESS THAN (9000000),
    PARTITION p9 VALUES LESS THAN (10000000),
    PARTITION p10 VALUES LESS THAN MAXVALUE
);


-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}tags`;
CREATE TABLE `{{$pk}}tags`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `tag_name`    varchar(20) NOT NULL COMMENT '分类名',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    `cover_url`   varchar(255) DEFAULT '' COMMENT '图片路径',
    PRIMARY KEY (`id`) USING BTREE,
    unique KEY `tag_name` (`tag_name`)
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for {{$pk}}friendship_link
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}friendship_link`;
CREATE TABLE `{{$pk}}friendship_link`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(100) NOT NULL COMMENT '友链名',
    `url`         varchar(255) NOT NULL COMMENT '友链地址',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for {{$pk}}area
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}area`;
CREATE TABLE `{{$pk}}area`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `area_name`   varchar(32) NOT NULL COMMENT '地区名',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    PRIMARY KEY (`id`),
    unique key `area_name` (`area_name`)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for {{$pk}}user_book
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}user_favor`;
CREATE TABLE `{{$pk}}user_favor`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `book_id`     int(10) unsigned NOT NULL COMMENT '用户收藏的漫画ID',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    `user_id`     int(10) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    key           book_id (`book_id`) USING BTREE,
    key           user_id (`user_id`) USING BTREE
) ENGINE=InnoDB  ROW_FORMAT=Dynamic;

-- ----------------------------
-- Table structure for {{$pk}}user_history
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}user_history`;
CREATE TABLE `{{$pk}}user_history`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `book_id`     int(10) unsigned NOT NULL COMMENT '用户阅读的漫画ID',
    `chapter_id`  int(10) unsigned NOT NULL COMMENT '用户阅读的章节ID',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    `user_id`     int(10) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    key           book_id (`book_id`) USING BTREE,
    key           user_id (`user_id`) USING BTREE
) ENGINE=InnoDB  ROW_FORMAT=Dynamic;

-- ----------------------------
-- Table structure for {{$pk}}comments
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}comments`;
CREATE TABLE `{{$pk}}comments`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(10) unsigned NOT NULL DEFAULT '0',
    `book_id`     int(10) unsigned NOT NULL DEFAULT '0',
    `content`     text,
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY           `user_id` (`user_id`),
    KEY           `book_id` (`book_id`)
) ENGINE=InnoDB ROW_FORMAT=Dynamic;

-- ----------------------------
-- Table structure for {{$pk}}article
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}article`;
CREATE TABLE `{{$pk}}article`
(
    `id`          int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `unique_id`   char(100)    DEFAULT NULL,
    `title`       varchar(200) NOT NULL,
    `content_url` varchar(200) NOT NULL,
    `cover_url`   varchar(255) DEFAULT '' COMMENT '封面图路径',
    `desc`        text,
    `book_id`     int(10) UNSIGNED DEFAULT NULL,
    `hits` int(10) unsigned DEFAULT '0' COMMENT '总人气',
    `mhits` int(10) unsigned DEFAULT '0' COMMENT '月人气',
    `whits` int(10) unsigned DEFAULT '0' COMMENT '周人气',
    `dhits` int(10) unsigned DEFAULT '0' COMMENT '日人气',
    `create_time` int(10) UNSIGNED DEFAULT NULL,
    `update_time` int(10) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY           `title` (`title`) USING BTREE,
    KEY           `book_id`(`book_id`) USING BTREE,
    KEY           `unique_id`(`unique_id`) USING BTREE,
    KEY               `hits`(`hits`) USING BTREE,
    KEY               `mhits`(`mhits`) USING BTREE,
    KEY               `whits`(`whits`) USING BTREE,
    KEY               `dhits`(`dhits`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for {{$pk}}vip_code
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}vip_code`;
CREATE TABLE `{{$pk}}vip_code`
(
    `id`          int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `code`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'vip码',
    `add_day`     int(10) unsigned DEFAULT 0 COMMENT '增加时间',
    `create_time` int(10) unsigned DEFAULT 0,
    `update_time` int(10) unsigned DEFAULT 0,
    `used`        tinyint(4) DEFAULT 1 COMMENT '1.未使用 2.已发出 3.已使用',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX         `code`(`code`) USING BTREE,
    INDEX         `used`(`used`) USING BTREE
) ENGINE=InnoDB ROW_FORMAT=Dynamic;

-- ----------------------------
-- Table structure for {{$pk}}charge_code
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}charge_code`;
CREATE TABLE `{{$pk}}charge_code`
(
    `id`          int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `code`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '卡密',
    `money`       decimal(5, 2)                                          NOT NULL COMMENT '面额',
    `used`        tinyint(4) DEFAULT 1 COMMENT '1.未使用 2.已发出 3.已使用',
    `create_time` int(10) unsigned NULL DEFAULT NULL,
    `update_time` int(10) unsigned NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    INDEX         `code`(`code`) USING BTREE,
    INDEX         `used`(`used`) USING BTREE
) ENGINE=InnoDB ROW_FORMAT=Dynamic;

DROP TABLE IF EXISTS `{{$pk}}book_logs`;
CREATE TABLE `{{$pk}}book_logs`
(
    `id`        int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `book_id`   int(10) UNSIGNED NOT NULL DEFAULT 0,
    `book_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci          DEFAULT '',
    `src_url`   varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    `log_time`  int(10) DEFAULT 0,
    `src`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX       `src_url`(`src_url`) USING BTREE,
    INDEX       `book_id`(`book_id`) USING BTREE,
    INDEX       `book_name`(`book_name`) USING BTREE,
    INDEX       `log_time`(`log_time`) USING BTREE,
    INDEX       `src`(`src`) USING BTREE
) ENGINE = InnoDB  ROW_FORMAT=Dynamic;

-- ----------------------------
-- Table structure for chapterlogs
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}chapter_logs`;
CREATE TABLE `{{$pk}}chapter_logs`
(
    `id`           int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `chapter_id`   int(10) UNSIGNED NOT NULL DEFAULT 0,
    `chapter_name` varchar(100) DEFAULT '',
    `src_url`      varchar(500)  NOT NULL DEFAULT '',
    `src`          varchar(32)  NOT NULL DEFAULT '',
    `log_time`     int(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`) USING BTREE,
    INDEX          `src_url`(`src_url`) USING BTREE,
    INDEX          `chapter_id`(`chapter_id`) USING BTREE,
    INDEX          `chapter_name`(`chapter_name`) USING BTREE,
    INDEX          `log_time`(`log_time`) USING BTREE,
    INDEX          `src`(`src`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT=Dynamic;

-- ----------------------------
-- Table structure for photologs
-- ----------------------------
DROP TABLE IF EXISTS `{{$pk}}photo_logs`;
CREATE TABLE `{{$pk}}photo_logs`
(
    `id`       int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `photo_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
    `src_url`  varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    `log_time` int(10) UNSIGNED DEFAULT 0,
    `src`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX      `src_url`(`src_url`) USING BTREE,
    INDEX      `photo_id`(`photo_id`) USING BTREE,
    INDEX      `log_time`(`log_time`) USING BTREE,
    INDEX      `src`(`src`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT=Dynamic;

DROP TABLE IF EXISTS `{{$pk}}tail`;
CREATE TABLE `{{$pk}}tail` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `book_id` int(10) unsigned NOT NULL DEFAULT '0',
    `tailname` varchar(200) NOT NULL COMMENT '长尾词',
    `tailcode` varchar(255) NOT NULL COMMENT '唯一标识',
    `create_time` int(10) unsigned DEFAULT '0',
    `update_time` int(10) unsigned DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `tailname` (`tailname`),
    unique key `tailcode` (`tailcode`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Records of {{$pk}}area
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}friendship_link` (`id`, `name`, `url`, `create_time`, `update_time`) VALUES (1, '聚合漫画网', 'https://www.52hah.com', 1669480562, 1669480562);
INSERT INTO `{{$pk}}area` (`id`, `area_name`, `create_time`, `update_time`) VALUES (1, '韩国', 1654493810, 1654493810);
INSERT INTO `{{$pk}}area` (`id`, `area_name`, `create_time`, `update_time`) VALUES (2, '日漫', 0, 0);
INSERT INTO `{{$pk}}area` (`id`, `area_name`, `create_time`, `update_time`) VALUES (3, '欧美', 0, 0);
INSERT INTO `{{$pk}}area` (`id`, `area_name`, `create_time`, `update_time`) VALUES (4, '港台漫', 0, 0);
INSERT INTO `{{$pk}}area` (`id`, `area_name`, `create_time`, `update_time`) VALUES (5, '国漫', 0, 0);
INSERT INTO `{{$pk}}area` (`id`, `area_name`, `create_time`, `update_time`) VALUES (10, '其他', 0, 0);
COMMIT;

-- ----------------------------
-- Records of {{$pk}}tags
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (1, '校园', 1654583262, 1654584875, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (2, '动作', 1654583385, 1654583385, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (4, '恐怖', 1654584893, 1654584893, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (5, '冒险', 1654584902, 1654584902, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (6, '恋爱', 1654584908, 1654584908, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (7, '剧情', 1654584965, 1654584965, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (8, '格斗', 1654827321, 1654827321, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (9, '治愈', 1654827326, 1654827326, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (10, '生活', 1654827330, 1654827330, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (11, '奇幻', 1654827346, 1654827346, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (12, '搞笑', 1654827349, 1654827349, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (13, '其他', 1654827352, 1654827352, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (14, '热血', 1654827375, 1654827375, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (15, '职场', 1654827381, 1654827381, '');
INSERT INTO `{{$pk}}tags` (`id`, `tag_name`, `create_time`, `update_time`, `cover_url`) VALUES (16, '神鬼', 1654827384, 1654827384, '');
COMMIT;

-- ----------------------------
-- Records of {{$pk}}book
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (1, 'yzqtc83de012d9be6f9bd8735f25faa477e8', '一周七兔_4', '一周七兔', 1, '第0话', 8, 1667395666, 1667395667, 1667395667, 0, '冒险|治愈|搞笑', '介绍:每周画7只兔子！ （一日一兔的排版有点问题 而且每天都发布有些麻烦 所以改成每周更新一次 每次更新七只）', 2, 1, '非影Q', 'https://images.dmzj.com/img/webpic/4/1003717441581819696.jpg', '', 9999, 0.00, 5, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (2, 'lpylkwlzywc6a8fb8047b33a063927947b50e52337', '老婆用连裤袜来治愈我_4', '老婆用连裤袜来治愈我', 325, '第18话', 25, 1667395737, 1667395798, 1667395798, 0, '欢乐向|爱情', '介绍:每天下班回到家都会很累 而我的妻子用不同方式治愈着我！！', 2, 2, 'ぐらんで', '/2/628b48cc0d4ca4c6.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (3, 'bcsdysjcjlrkrdswyxccb067503c29319fd4dae6d90340b284', '被传送到异世界参加令人困扰的死亡游戏_4', '被传送到异世界参加令人困扰的死亡游戏', 327, '第17话', 18, 1667395737, 1667395798, 1667395798, 0, '冒险|欢乐向|魔幻|生存', '介绍:打起精神、开始迎接崭新的每一天♪死神的里面居然是这样的！哦哦哦！太......', 1, 3, '水あさと', '/3/7ecfa2278c1da9e8.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (4, 'zqdzybsyzybsxzhxsjdswdyz8d9d3550552b16dae78e53ab3a16d924', '最强的职业不是勇者也不是贤者好像是鉴定士(伪)的样子_4', '最强的职业不是勇者也不是贤者好像是鉴定士(伪)的样子', 316, '第16.1话', 19, 1667395737, 1667395796, 1667395796, 0, '冒险|欢乐向|爱情|轻小说', '介绍:突然被转移到异世界的高中生「 响 」，在四处都有魔物栖息的奇幻世界中、独自一个人在广大草原上徬徨，且一开始就持有的技能竟然是以「 鉴定 」为首的一系列非战斗技能！？幸好在一场突发战斗中却意外让响学会了不同的用法，一场奇迹般的异世界冒险谭就此开幕！', 2, 4, 'あてきち/武田充司', 'https://images.dmzj.com/webpic/0/zuiqiangzhiyebushiyongzheshijiandingshi.png', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (5, 'ssjnzszhhds89c869e55cb8be11391807516c92916a', '叔叔教女●中生做坏坏的事_4', '叔叔教女●中生做坏坏的事', 314, '第17话', 28, 1667395738, 1667395796, 1667395796, 0, '欢乐向', '介绍:叔叔，可以教我学校里学不到的事情吗？人家想当个坏孩子——', 1, 5, '久川はる', 'https://images.dmzj.com/webpic/0/ssjngzszhuaihuaidshi.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (6, 'nywzhdzcyhssjqsdsz53fce56ae42223f59dadf9ce20150d8a', '你与我最后的战场，亦或是世界起始的圣战_4', '你与我最后的战场，亦或是世界起始的圣战', 329, '第16话', 44, 1667395738, 1667395799, 1667395799, 0, '科幻|魔法|轻小说', '介绍:有着高度科学力的帝国，和因「魔女之国」而被畏惧的涅比里斯皇厅。在这永远持续下去的战场上，少年和少女相遇了。史上最年少而有着帝国最高战力的剑士——伊斯卡。皇厅最强的冰之魔女姬——爱丽丝莉泽。「能俘虏我的话，你的梦想说不定也能实现呢」「你才是，只要打倒我的话就好了，你就能向着统一世界前进了」，作为宿敌而互相厮杀的两人。但是，少年被少女的美丽和高洁夺取心神，少女被少年的强大和他的生存方式所吸引。', 2, 6, 'okama/细音启', 'https://images.dmzj.com/webpic/3/191226nywzhdzcyszhdsz.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (7, 'df5b11823f8b9d9ba5211c6c54f79573', 'The New Gate_4', 'The New Gate', 328, '第17话', 49, 1667395738, 1667395798, 1667395798, 0, '冒险|科幻|轻小说', '介绍:在几万人被卷入的在线死亡游戏游戏「THE NEW GATE」(这......)，最老玩家·辛格的活跃，终于解放的时候，迎接。但是，在最后的BOSS打倒后想要逃离的瞬间，突然未知的光芒照射，被传送到游戏中 500年后的世界！最强的玩家的新的无双战斗传说开幕！', 2, 7, '风波しのぎ/三轮ヨツユキ', 'https://images.dmzj.com/webpic/5/thenewgate.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (8, 'pfsbmhgq330697ba8238acc70ce2dd26737ef99f', '平凡士兵梦回过去_4', '平凡士兵梦回过去', 315, '第16话', 62, 1667395738, 1667395796, 1667395796, 0, '冒险|魔法|轻小说', '介绍:士兵在看到魔王被干掉的那一刻，被人从背后捅死了，结果莫名其妙穿越时空，回到自己还是婴儿的时期……', 2, 8, '丘野优/铃木イゾ', 'https://images.dmzj.com/webpic/4/pingfanshibingmenghuiguoqu.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (9, 'zysjklgeydbzwhmyygrxlk05c0b9fa4189b67bec73b5f88f8bfcd5', '在异世界开了孤儿院，但不知为何没有一个人想离开_4', '在异世界开了孤儿院，但不知为何没有一个人想离开', 312, '第15话', 34, 1667395738, 1667395796, 1667395796, 0, '轻小说', '介绍:突然开始在异世界经营起孤儿院了？', 2, 9, '有池智实/初枝れんげ', 'https://images.dmzj.com/webpic/8/zysjklgey489569l.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (10, 'xsdyjsdab8126f2b0c4487f937d65646690b95', '幸色的一居室_4', '幸色的一居室', 319, '第15话', 61, 1667395739, 1667395797, 1667395797, 0, '爱情|治愈', '介绍:那天，我的人生被诱拐犯拯救了。我与他定下结婚的誓言，他为我捧来奢望般的幸福。诱拐犯与被害者的生活，却弥漫着淡淡温情。pixiv和Twitter的超话题作！！！', 2, 10, 'はくり', 'https://images.dmzj.com/webpic/13/180822xsdyjs.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (11, 'wjnpsbt617d6fd5d57a6a8d88bb317ebf196929', '我家女仆是变态_4', '我家女仆是变态', 318, '第17话', 111, 1667395739, 1667395797, 1667395797, 0, '欢乐向', '介绍:我家女仆是个变态，她会在我睡懒觉时候给我个早安咬叫我起床！', 2, 11, 'saku', 'https://images.dmzj.com/webpic/1/meidoushibiantai0415.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (12, 'zctxyndzmo15c2da2ff83436adce9f24f97ec264cf', '佐仓同学有你的指名哦_4', '佐仓同学有你的指名哦', 324, '第15话', 23, 1667395739, 1667395798, 1667395798, 0, '欢乐向|秀吉', '介绍:打架超~厉害、男人中の男人的佐仓君在女仆咖啡店的兼职日常！！', 1, 12, '森みさき', 'https://images.dmzj.com/webpic/15/zuocangtongxueyounidezhimingo.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (13, 'lyyhbhs19d4e4257104064db6a59ec2555f49ed', '老爷爷还不会死_4', '老爷爷还不会死', 313, '第14话', 25, 1667395739, 1667395796, 1667395796, 0, '欢乐向|爱情|奇幻', '介绍:压倒性的战斗力量，不曾失败的花花公子，这个老爷爷是最强的!很久以前,拯救了国家后被称为英雄,是人们的敬仰的男人。现在这份荣誉被人们遗忘,衰弱的老人孤独地在深山里生活着……这时有一位天使，前来治愈活在寂寞之中的他……但是……在她面前的是——?', 1, 13, 'まどろみ太郎', 'https://images.dmzj.com/webpic/15/caibushilaoyeye.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (14, 'jblddl961d4d07ef425496bf050cd047b877aa', '珈百璃的堕落_4', '珈百璃的堕落', 317, '第10话', 103, 1667395740, 1667395797, 1667395797, 0, '欢乐向|ゆり', '介绍:三好天使来到人间，结果变成废材天使！？', 2, 14, 'うかみ', 'https://images.dmzj.com/webpic/0/t3896223duoluodejbl680001.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (15, 'clbmwdrcc5d7c85dbe1303d53f0ff3339fc9825', '重来吧、魔王大人！_4', '重来吧、魔王大人！', 326, '第16话', 33, 1667395740, 1667395798, 1667395798, 0, '冒险|魔法|轻小说', '介绍:随处可见的社会人士——大野晶（おおの　あきら）在自己运营的游戏内，以被称之为「魔王」角色的登陆状态下，转移到了异世界。 　他在这里遇到了一位右脚不便的小孩子，并一同启程旅行。然而「魔王」持有压倒性的力量，其他人不可能放任不管。 　他们被想要讨伐魔王的国家和圣女所盯上，这场引发的骚动最终都把整座王国卷入其中。 　这是一部外观上是魔王，可其内在是一般人的误会系幻想世界。', 2, 15, '神埼黑音/身ノ丈あまる', 'https://images.dmzj.com/webpic/3/chonglaiamowangdare.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (16, 'mymzdgw03f174319420be19b48cd0a51d3723a5', '没有名字的怪物_4', '没有名字的怪物', 310, '第13话', 68, 1667395740, 1669351023, 1667395795, 0, '悬疑|轻小说', '介绍:突然出现的少女【怪物】，侵食著「我」的身与心，以及少女身边相继发生的连续猎奇杀人事件……。少女的真面目、目的究竟是……？ 一旦开始阅读此漫画，就再也无法逃离它的魅力，货真价实的“怪物”作品！', 1, 16, '万丈梓/黑木京也', 'https://images.dmzj.com/webpic/10/mymzdgw20200504.jpg', '.', 9999, 0.00, 2, 0, NULL, 0, 1, 1, 1, 1);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (17, 'qnhqbajjtx40f78d90c1043589ef8bb975399bf9e2', '请你回去吧！阿久津同学_4', '请你回去吧！阿久津同学', 320, '第15.5话', 139, 1667395740, 1667395798, 1667395798, 0, '欢乐向|爱情', '介绍:不喜欢上课的不良女孩·阿久津同学，不知为何总是往一个人住的大山同学家里跑？面对这种无防备无距离感的女生，明明想让她回去，却又希望她留下？？发生在同一屋檐下的校园短喜剧！', 2, 17, '长冈太一', 'https://images.dmzj.com/webpic/5/qingnihuiqubaajiujintongxue530.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (18, 'oxyszdlxgx0711914aaf33f8eee3d07abd29af44f3', '偶像与死宅的理想关系_4', '偶像与死宅的理想关系', 322, '第15话', 109, 1667395740, 1667395798, 1667395798, 0, '欢乐向|爱情', '介绍:偶像住在死宅的旁边会发生什么样的故事呢？', 2, 18, '脊髄引き抜きの刑', 'https://images.dmzj.com/webpic/0/oxyszdlxgx2020130.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (19, 'dystqdrca58007d80896aeb8e5f5cc6f9a06fb80', '地狱三头犬的日常_4', '地狱三头犬的日常', 311, '第14话', 64, 1667395741, 1667395795, 1667395795, 0, '欢乐向|爱情|泛爱', '介绍:以短篇博得大人气，萌系猛犬的爱情喜剧，诚意满满连载开始！\n一个身体里寄托着三份思念！地狱的猛犬，寸步不离！', 1, 19, '樱井亚都', 'https://images.dmzj.com/webpic/5/diyusantouquanderichangV2.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (20, 'zqmfsdydjh5443d9c1e97bad0059510ad71ee54402', '最强魔法师的隐遁计划_4', '最强魔法师的隐遁计划', 259, '第13话', 13, 1667395741, 1667395786, 1667395786, 0, '爱情|魔法|校园|轻小说', '介绍:一位君临十万人之上的天才魔法师阿卢斯以普通学生的身份进入魔法学院，这之后到底会掀起怎么样的波澜呢？', 1, 20, 'うおぬまゆう/イズシロ', 'https://images.dmzj.com/webpic/3/zuiqiangmofashiyidujihua.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (21, 'xzzmmzsxgdyz62a0391995c593e5a68eeb8b8ef6df10', '想治治妹妹这死小鬼的样子！_4', '想治治妹妹这死小鬼的样子！', 323, '第17话', 33, 1667395741, 1667395798, 1667395798, 0, '欢乐向|萌系', '介绍:妹妹和以前的乖巧样子差太多了,要治治?', 2, 21, '徳之ゆいか(とくの)', 'https://images.dmzj.com/webpic/16/xzzmmzsxgdyz20200817.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
INSERT INTO `{{$pk}}book` (`id`, `unique_id`, `book_name`, `nick_name`, `last_chapter_id`, `last_chapter`, `chapter_count`, `create_time`, `update_time`, `last_time`, `delete_time`, `tags`, `summary`, `end`, `author_id`, `author_name`, `cover_url`, `banner_url`, `start_pay`, `money`, `area_id`, `is_top`, `src_url`, `is_copyright`, `hits`, `mhits`, `whits`, `dhits`) VALUES (22, 'ygwqd4936b108afff73e6cfa86b0c83a3e94', '迎鬼为妻_4', '迎鬼为妻', 321, '第03话', 12, 1667395790, 1667395798, 1667395798, 0, '欢乐向|爱情', '介绍:桃太郎从鬼岛回来了!带回来的不是狗，猴，鸡而竟是鬼新娘！！由旁若无人口出恶言，还有些工口的鬼族公主引起的恋爱（？）喜剧！', 2, 22, 'あわ箱', 'https://images.dmzj.com/webpic/10/ygwq718.jpg', '', 9999, 0.00, 2, 0, NULL, 0, 0, 0, 0, 0);
COMMIT;

-- ----------------------------
-- Records of {{$pk}}chapter
-- ----------------------------
BEGIN;
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (1, '第0话', 1667395667, 1667395667, 1, 0.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (2, '第01话', 1667395737, 1667395737, 2, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (3, '第01话', 1667395737, 1667395737, 3, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (4, '第01话', 1667395737, 1667395737, 4, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (5, '第01话', 1667395738, 1667395738, 5, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (6, '第00话', 1667395738, 1667395738, 6, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (7, '第01话', 1667395738, 1667395738, 7, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (8, '第01话', 1667395738, 1667395738, 8, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (9, '第01话', 1667395738, 1667395738, 9, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (10, '单行本01话', 1667395739, 1667395739, 10, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (11, '第01话', 1667395739, 1667395739, 11, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (12, '短篇1', 1667395739, 1667395739, 12, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (13, '第01话', 1667395739, 1667395739, 13, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (14, '第01卷', 1667395740, 1667395740, 14, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (15, '第01话', 1667395740, 1667395740, 15, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (16, '第1.1话', 1667395740, 1667395740, 16, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (17, '第01话', 1667395740, 1667395740, 17, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (18, '第01话', 1667395741, 1667395741, 18, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (19, '第02话', 1667395741, 1667395741, 2, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (20, '第00话', 1667395741, 1667395741, 19, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (21, '第01话', 1667395741, 1667395741, 20, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (22, '第01话', 1667395742, 1667395742, 21, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (23, '第02话', 1667395742, 1667395742, 3, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (24, '第02话', 1667395742, 1667395742, 4, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (25, '第02话', 1667395742, 1667395742, 5, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (26, '第01话', 1667395742, 1667395742, 6, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (27, '第02话', 1667395743, 1667395743, 7, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (28, '第02话', 1667395743, 1667395743, 8, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (29, '第02话', 1667395743, 1667395743, 9, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (30, '第02话', 1667395743, 1667395743, 11, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (31, '单行本02话', 1667395744, 1667395744, 10, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (32, '第01话', 1667395744, 1667395744, 12, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (33, '第02话', 1667395744, 1667395744, 13, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (34, '第1.2话', 1667395744, 1667395744, 16, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (35, '第02话', 1667395744, 1667395744, 17, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (36, '第02话', 1667395744, 1667395744, 15, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (37, '第02话', 1667395744, 1667395744, 18, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (38, '第03话', 1667395745, 1667395745, 2, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (39, '第02话', 1667395745, 1667395745, 21, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (40, '第02卷', 1667395745, 1667395745, 14, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (41, '第02话', 1667395745, 1667395745, 20, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (42, '第01话', 1667395745, 1667395745, 19, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (43, '第03话', 1667395745, 1667395745, 5, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (44, '第03话', 1667395746, 1667395746, 3, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (45, '第03话', 1667395746, 1667395746, 4, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (46, '第02话', 1667395746, 1667395746, 6, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (47, '第03话', 1667395746, 1667395746, 7, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (48, '第03话', 1667395747, 1667395747, 9, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (49, '第03话', 1667395747, 1667395747, 11, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (50, '第03话', 1667395747, 1667395747, 8, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (51, '第02话', 1667395747, 1667395747, 12, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (52, '单行本03话', 1667395747, 1667395747, 10, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (53, '第03话', 1667395748, 1667395748, 13, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (54, '第03话', 1667395748, 1667395748, 17, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (55, '第1.3话', 1667395748, 1667395748, 16, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (56, '第03话', 1667395748, 1667395748, 18, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (57, '第03话', 1667395748, 1667395748, 15, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (58, '第04话', 1667395749, 1667395749, 2, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (59, '第03话', 1667395749, 1667395749, 21, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (60, '第03话', 1667395749, 1667395749, 20, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (61, '第04话', 1667395749, 1667395749, 5, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (62, '第04话', 1667395749, 1667395749, 3, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (63, '第03卷', 1667395750, 1667395750, 14, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (64, '第02话', 1667395750, 1667395750, 19, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (65, '第04话', 1667395750, 1667395750, 4, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (66, '第04话', 1667395750, 1667395750, 7, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (67, '第03话', 1667395750, 1667395750, 6, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (68, '第04话', 1667395751, 1667395751, 11, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (69, '第04话', 1667395751, 1667395751, 9, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (70, '第03话', 1667395751, 1667395751, 12, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (71, '第04话', 1667395752, 1667395752, 8, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (72, '单行本04话', 1667395752, 1667395752, 10, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (73, '第04话', 1667395752, 1667395752, 17, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (74, '第04话', 1667395752, 1667395752, 13, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (75, '第04话', 1667395752, 1667395752, 18, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (76, '第02话', 1667395753, 1667395753, 16, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (77, '第04话', 1667395753, 1667395753, 15, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (78, '第04话', 1667395753, 1667395753, 21, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (79, '第05话', 1667395753, 1667395753, 2, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (80, '第05话', 1667395753, 1667395753, 5, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (81, '第04话', 1667395754, 1667395754, 20, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (82, '第05话', 1667395754, 1667395754, 3, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (83, '第03话', 1667395754, 1667395754, 19, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (84, '第05话', 1667395754, 1667395754, 7, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (85, '第05话', 1667395754, 1667395754, 4, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (86, '第04话', 1667395755, 1667395755, 6, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (87, '第05话', 1667395755, 1667395755, 11, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (88, '第04卷', 1667395755, 1667395755, 14, 4.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (89, '第05话', 1667395755, 1667395755, 9, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (90, '第04话', 1667395755, 1667395755, 12, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (91, '第05话', 1667395756, 1667395756, 8, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (92, '单行本05话', 1667395756, 1667395756, 10, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (93, '第05话', 1667395756, 1667395756, 17, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (94, '第05话', 1667395756, 1667395756, 18, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (95, '第05话', 1667395757, 1667395757, 13, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (96, '第05话', 1667395757, 1667395757, 21, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (97, '第05话', 1667395757, 1667395757, 15, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (98, '第06话', 1667395757, 1667395757, 2, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (99, '第06话', 1667395757, 1667395757, 5, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (100, '第03话', 1667395757, 1667395757, 16, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (101, '第05话', 1667395757, 1667395757, 20, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (102, '第06话', 1667395758, 1667395758, 3, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (103, '第04话', 1667395758, 1667395758, 19, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (104, '第06话', 1667395758, 1667395758, 7, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (105, '第06话', 1667395759, 1667395759, 4, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (106, '第06话', 1667395759, 1667395759, 11, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (107, '第05话', 1667395759, 1667395759, 6, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (108, '第05话', 1667395759, 1667395759, 12, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (109, '第06话', 1667395759, 1667395759, 9, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (110, '第06话', 1667395760, 1667395760, 17, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (111, '第06话', 1667395760, 1667395760, 8, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (112, '单行本06话', 1667395760, 1667395760, 10, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (113, '第06话', 1667395760, 1667395760, 18, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (114, '第06话', 1667395760, 1667395760, 21, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (115, '第05卷', 1667395760, 1667395760, 14, 5.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (116, '第06话', 1667395761, 1667395761, 13, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (117, '第07话', 1667395761, 1667395761, 2, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (118, '第06话', 1667395761, 1667395761, 15, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (119, '第07话', 1667395761, 1667395761, 5, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (120, '第04话', 1667395762, 1667395762, 16, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (121, '第06话', 1667395762, 1667395762, 20, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (122, '第07话', 1667395762, 1667395762, 3, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (123, '第05话', 1667395762, 1667395762, 19, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (124, '第07话', 1667395762, 1667395762, 7, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (125, '第07话', 1667395763, 1667395763, 4, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (126, '第07话', 1667395763, 1667395763, 11, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (127, '第06话', 1667395763, 1667395763, 12, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (128, '第06话', 1667395763, 1667395763, 6, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (129, '第07话', 1667395763, 1667395763, 9, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (130, '第07话', 1667395763, 1667395763, 17, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (131, '第07话', 1667395764, 1667395764, 8, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (132, '第07话', 1667395764, 1667395764, 18, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (133, '七夕贺图', 1667395764, 1667395764, 10, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (134, '第07话', 1667395764, 1667395764, 21, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (135, '第07话', 1667395764, 1667395764, 13, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (136, '第08话', 1667395765, 1667395765, 5, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (137, '第08话', 1667395765, 1667395765, 2, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (138, '第07话', 1667395765, 1667395765, 15, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (139, '第07话', 1667395765, 1667395765, 20, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (140, '第05话', 1667395765, 1667395765, 16, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (141, '第01话', 1667395765, 1667395765, 14, 6.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (142, '第08话', 1667395765, 1667395765, 3, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (143, '第06话', 1667395766, 1667395766, 19, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (144, '第08话', 1667395766, 1667395766, 11, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (145, '第08话', 1667395766, 1667395766, 7, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (146, '第07话', 1667395766, 1667395766, 12, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (147, '第08话', 1667395766, 1667395766, 4, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (148, '第07话', 1667395767, 1667395767, 6, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (149, '第7.5话', 1667395767, 1667395767, 17, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (150, '第08话', 1667395767, 1667395767, 9, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (151, '第08话', 1667395767, 1667395767, 18, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (152, '第08话', 1667395767, 1667395767, 8, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (153, '单行本07话', 1667395767, 1667395767, 10, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (154, '第08话', 1667395768, 1667395768, 21, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (155, '第09话', 1667395768, 1667395768, 2, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (156, '第08话', 1667395768, 1667395768, 13, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (157, '第09话', 1667395768, 1667395768, 5, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (158, '第08话', 1667395768, 1667395768, 15, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (159, '第08话', 1667395768, 1667395768, 20, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (160, '第02话', 1667395769, 1667395769, 14, 7.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (161, '第09话', 1667395769, 1667395769, 3, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (162, '第06话', 1667395769, 1667395769, 16, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (163, '第09话', 1667395769, 1667395769, 11, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (164, '第07话', 1667395769, 1667395769, 19, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (165, '第08话', 1667395769, 1667395769, 12, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (166, '第09话', 1667395770, 1667395770, 7, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (167, '第09话', 1667395770, 1667395770, 4, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (168, '第08话', 1667395770, 1667395770, 17, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (169, '第08话', 1667395770, 1667395770, 6, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (170, '第09话', 1667395770, 1667395770, 18, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (171, '第09话', 1667395771, 1667395771, 9, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (172, '第09话', 1667395771, 1667395771, 8, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (173, '第09话', 1667395771, 1667395771, 21, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (174, '单行本08话', 1667395771, 1667395771, 10, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (175, '第10话', 1667395771, 1667395771, 2, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (176, '第09话', 1667395771, 1667395771, 13, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (177, '第10话', 1667395771, 1667395771, 5, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (178, '第09话', 1667395771, 1667395771, 15, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (179, '第09话', 1667395772, 1667395772, 20, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (180, '第03话', 1667395772, 1667395772, 14, 8.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (181, '第10话', 1667395772, 1667395772, 3, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (182, '第10话', 1667395772, 1667395772, 11, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (183, '第07话', 1667395772, 1667395772, 16, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (184, '第09话', 1667395773, 1667395773, 12, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (185, '第08话', 1667395773, 1667395773, 19, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (186, '第10话', 1667395773, 1667395773, 7, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (187, '第09话', 1667395773, 1667395773, 17, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (188, '第10话', 1667395774, 1667395774, 4, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (189, '第10话', 1667395774, 1667395774, 18, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (190, '第09话', 1667395774, 1667395774, 6, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (191, '第10话', 1667395774, 1667395774, 21, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (192, '第10话', 1667395774, 1667395774, 9, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (193, '第10话', 1667395775, 1667395775, 8, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (194, '第11话', 1667395775, 1667395775, 2, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (195, '第10话', 1667395775, 1667395775, 13, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (196, '单行本09话', 1667395775, 1667395775, 10, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (197, '第11话', 1667395775, 1667395775, 5, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (198, '第10话', 1667395775, 1667395775, 15, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (199, '第10话', 1667395775, 1667395775, 20, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (200, '第04话', 1667395776, 1667395776, 14, 9.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (201, '第11话', 1667395776, 1667395776, 11, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (202, '第11话', 1667395776, 1667395776, 3, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (203, '第08话', 1667395777, 1667395777, 16, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (204, '第10话', 1667395777, 1667395777, 12, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (205, '第10话', 1667395777, 1667395777, 17, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (206, '第11话', 1667395777, 1667395777, 7, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (207, '第09话', 1667395777, 1667395777, 19, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (208, '第11话', 1667395777, 1667395777, 4, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (209, '第11话', 1667395778, 1667395778, 18, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (210, '第11话', 1667395778, 1667395778, 21, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (211, '第10话', 1667395778, 1667395778, 6, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (212, '第11话', 1667395778, 1667395778, 9, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (213, '第12话', 1667395778, 1667395778, 2, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (214, '第10.5话', 1667395778, 1667395778, 13, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (215, '第11话', 1667395778, 1667395778, 8, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (216, '单行本10话', 1667395779, 1667395779, 10, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (217, '第12话', 1667395779, 1667395779, 5, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (218, '第11话', 1667395779, 1667395779, 20, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (219, '第05话', 1667395779, 1667395779, 14, 10.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (220, '第11话', 1667395780, 1667395780, 15, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (221, '第12话', 1667395780, 1667395780, 11, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (222, '第12话', 1667395780, 1667395780, 3, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (223, '第11话', 1667395780, 1667395780, 17, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (224, '短篇2', 1667395780, 1667395780, 12, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (225, '第12话', 1667395781, 1667395781, 7, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (226, '第09话', 1667395781, 1667395781, 16, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (227, '第12话', 1667395781, 1667395781, 18, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (228, '第10话', 1667395781, 1667395781, 19, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (229, '第12话', 1667395781, 1667395781, 21, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (230, '第12话', 1667395782, 1667395782, 4, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (231, '第11话', 1667395782, 1667395782, 6, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (232, '第12话', 1667395782, 1667395782, 9, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (233, '第13话', 1667395782, 1667395782, 2, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (234, '第11话', 1667395782, 1667395782, 13, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (235, '第12话', 1667395783, 1667395783, 8, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (236, '第13话', 1667395783, 1667395783, 5, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (237, '单行本11话', 1667395783, 1667395783, 10, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (238, '第12话', 1667395783, 1667395783, 20, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (239, '第06话', 1667395783, 1667395783, 14, 11.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (240, '第12话', 1667395783, 1667395783, 15, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (241, '第13话', 1667395783, 1667395783, 11, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (242, '第12话', 1667395783, 1667395783, 17, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (243, '第13话', 1667395784, 1667395784, 3, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (244, '第11话', 1667395784, 1667395784, 12, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (245, '第13话', 1667395784, 1667395784, 7, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (246, '第12.5话', 1667395784, 1667395784, 18, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (247, '第10话', 1667395784, 1667395784, 16, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (248, '第11话', 1667395784, 1667395784, 19, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (249, '第13话', 1667395785, 1667395785, 21, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (250, '第14话', 1667395785, 1667395785, 2, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (251, '第12话', 1667395785, 1667395785, 6, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (252, '第13话', 1667395785, 1667395785, 9, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (253, '第13话', 1667395786, 1667395786, 4, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (254, '特典', 1667395786, 1667395786, 13, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (255, '第14话', 1667395786, 1667395786, 5, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (256, '第13话', 1667395786, 1667395786, 8, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (257, '单行本12话', 1667395786, 1667395786, 10, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (258, '第07话', 1667395786, 1667395786, 14, 12.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (259, '第13话', 1667395786, 1667395786, 20, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (260, '第14话', 1667395787, 1667395787, 11, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (261, '第13话', 1667395787, 1667395787, 17, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (262, '第13话', 1667395787, 1667395787, 15, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (263, '第12话', 1667395787, 1667395787, 12, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (264, '第14话', 1667395787, 1667395787, 3, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (265, '第13话', 1667395787, 1667395787, 18, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (266, '第14话', 1667395788, 1667395788, 21, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (267, '第14话', 1667395788, 1667395788, 7, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (268, '第11话', 1667395788, 1667395788, 16, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (269, '第12话', 1667395788, 1667395788, 19, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (270, '第15话', 1667395788, 1667395788, 2, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (271, '第13话', 1667395788, 1667395788, 6, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (272, '番外01', 1667395789, 1667395789, 9, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (273, '第12话', 1667395789, 1667395789, 13, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (274, '第14话', 1667395789, 1667395789, 4, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (275, '第15话', 1667395789, 1667395789, 5, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (276, '第14话', 1667395789, 1667395789, 8, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (277, '单行本13话', 1667395790, 1667395790, 10, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (278, '第08话', 1667395790, 1667395790, 14, 13.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (279, '第15话', 1667395790, 1667395790, 11, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (280, '第14话', 1667395790, 1667395790, 17, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (281, '第01话', 1667395790, 1667395790, 22, 1.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (282, '第14话', 1667395791, 1667395791, 15, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (283, '第13话', 1667395791, 1667395791, 12, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (284, '第14话', 1667395791, 1667395791, 18, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (285, '第15话', 1667395791, 1667395791, 3, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (286, '第15话', 1667395791, 1667395791, 21, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (287, '第16话', 1667395791, 1667395791, 2, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (288, '第15话', 1667395791, 1667395791, 7, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (289, '第12话', 1667395792, 1667395792, 16, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (290, '第13话', 1667395792, 1667395792, 19, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (291, '第14话', 1667395792, 1667395792, 6, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (292, '第14话', 1667395792, 1667395792, 9, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (293, '第13话', 1667395792, 1667395792, 13, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (294, '第16话', 1667395793, 1667395793, 5, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (295, '第15话', 1667395793, 1667395793, 4, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (296, '第15话', 1667395793, 1667395793, 8, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (297, '第09话', 1667395793, 1667395793, 14, 14.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (298, '第14话', 1667395793, 1667395793, 10, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (299, '第16话', 1667395793, 1667395793, 11, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (300, '第15话', 1667395794, 1667395794, 17, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (301, '第02话', 1667395794, 1667395794, 22, 2.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (302, '第14.5话', 1667395794, 1667395794, 18, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (303, '第14话', 1667395794, 1667395794, 12, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (304, '第16话', 1667395794, 1667395794, 21, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (305, '第15话', 1667395794, 1667395794, 15, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (306, '第16话', 1667395794, 1667395794, 3, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (307, '第17话', 1667395795, 1667395795, 2, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (308, '第16话', 1667395795, 1667395795, 7, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (309, '第15话', 1667395795, 1667395795, 6, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (310, '第13话', 1667395795, 1667395795, 16, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (311, '第14话', 1667395795, 1667395795, 19, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (312, '第15话', 1667395796, 1667395796, 9, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (313, '第14话', 1667395796, 1667395796, 13, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (314, '第17话', 1667395796, 1667395796, 5, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (315, '第16话', 1667395796, 1667395796, 8, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (316, '第16.1话', 1667395796, 1667395796, 4, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (317, '第10话', 1667395797, 1667395797, 14, 15.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (318, '第17话', 1667395797, 1667395797, 11, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (319, '第15话', 1667395797, 1667395797, 10, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (320, '第15.5话', 1667395798, 1667395798, 17, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (321, '第03话', 1667395798, 1667395798, 22, 3.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (322, '第15话', 1667395798, 1667395798, 18, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (323, '第17话', 1667395798, 1667395798, 21, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (324, '第15话', 1667395798, 1667395798, 12, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (325, '第18话', 1667395798, 1667395798, 2, 18.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (326, '第16话', 1667395798, 1667395798, 15, 16.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (327, '第17话', 1667395798, 1667395798, 3, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (328, '第17话', 1667395798, 1667395798, 7, 17.00);
INSERT INTO `{{$pk}}chapter` (`id`, `chapter_name`, `create_time`, `update_time`, `book_id`, `chapter_order`) VALUES (329, '第16话', 1667395799, 1667395799, 6, 17.00);
COMMIT;

