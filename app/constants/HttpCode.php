<?php

declare(strict_types=1);

namespace app\constants;


/**
 * @Constants
 */
class HttpCode
{
    /**
     * @Message("Server Error！")
     */
    const SERVER_ERROR = 500;

    /**
     * @Message("操作成功")
     */
    const SUCCESS = 200;

    /**
     * @Message("方法不存在!")
     */
    const ERR_NON_EXISTENT = 404;

    /**
     * @Message("登录信息已失效")
     */
    const ERR_NOT_AUTH = 401;
    
}
