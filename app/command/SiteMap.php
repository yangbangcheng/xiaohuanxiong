<?php


namespace app\command;

use app\madmin\service\AddonsData;
use app\utils\RedisUtil;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Exception;
use think\facade\App;
use think\facade\Db;

class SiteMap extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('genSiteMap')
            ->addArgument('limit',Argument::REQUIRED,'单文件多少链接')
            ->setDescription('生成站点地图');
    }

    protected function execute(Input $input, Output $output)
    {
        $pagesize=40000;
        $url="";
        $map_dir="/master_sitemap";
        $sitemap= new \app\admin\service\sitemap($url,$map_dir);
        $sitemap->gen($pagesize,"book","pc");

        $output->writeln("测试");
    }

}