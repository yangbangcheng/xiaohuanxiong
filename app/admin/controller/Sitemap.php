<?php


namespace app\admin\controller;

use app\model\Article;
use app\model\Book;
use app\model\Chapter;
use think\facade\App;

class Sitemap extends BaseAdmin
{
    public function index()
    {
        if (request()->isPost()) {
            $pagesize = input('pagesize');
            $part = input('part');
            $end = input('end');

            $sitemap_service=new \app\admin\service\sitemap();

            $sitemap_service->gen($pagesize, $part, $end);
        }
        return view();
    }


}